package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.*;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GameHistoryMockTestController {

    Player firstPlayer = new Player(1L, "Alex", "111");
    Game game1 = new Game(firstPlayer, GameType.RSP);
    Long gameId = 8L;
    GameHistory gameHistory = new GameHistory(game1);
    List<TurnLog> gameTurns = Arrays.asList(new TurnLog(gameId, firstPlayer.getLogin(), "ROCK"));

    private GameHistoryService gameHistoryService;
    private GameHistoryController controller;

    @Before
    public void init() {
        gameHistoryService = Mockito.mock(GameHistoryService.class);
        controller = new GameHistoryController(gameHistoryService);
    }

    @Test
    public void testingIfReturnsTurnLogsAndRenderingToJsp() {
        //setup
        Mockito.when( gameHistoryService.getGameHistoryById(gameId)).thenReturn(gameHistory);
        GameType gameType = gameHistory.getGameType();
        Mockito.when(gameHistoryService.getTurnsByGameId(gameId)).thenReturn(gameTurns);
        ModelMap model = new ModelMap();
        //Execute
        String viewName =
                controller.getGameHistory( gameId, model);
        ///Verify
        assertEquals(viewName, "gameHistory");
        assertEquals(model.get("gameType"), gameType);
        assertEquals(model.get("gameId"),gameId);
        assertEquals(model.get("gameTurns"), gameTurns);
        assertEquals(model.get("gameHistory"), gameHistory);
    }


}

