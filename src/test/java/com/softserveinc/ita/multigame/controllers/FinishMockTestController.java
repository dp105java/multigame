package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.controllers.halma.HalmaPageController;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.GameListService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class FinishMockTestController {

    Player player = new Player(12L, "Black Jack", "111");
    Long gameId = 5L;
    Locale locale;
    String win = "Win!";

    private GameHistoryService gameHistoryService;
    private MessageSource messageSource;
    private FinishController finishController;

    @Before
    public void init() {
        gameHistoryService = Mockito.mock(GameHistoryService.class);
        messageSource = Mockito.mock(MessageSource.class);
        finishController = new FinishController(gameHistoryService, messageSource);
    }
    @Test
    public void testingReturningToFinishJsp() {
        //Setup
        Mockito.when(messageSource.getMessage("label.finish.win", new Object[0], locale)).thenReturn(win);
        ModelMap model = new ModelMap();
        //Execute
        String viewName = finishController.finish(player, model, gameId, locale);
        //Verify
        assertEquals(viewName, "finish");
    }
}
