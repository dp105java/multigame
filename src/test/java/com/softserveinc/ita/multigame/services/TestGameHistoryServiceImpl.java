package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.repositories.GameHistoryRepository;
import com.softserveinc.ita.multigame.repositories.TurnLogRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestGameHistoryServiceImpl {
    @InjectMocks
    GameHistoryService gameHistoryServiceImpl = new GameHistoryServiceImpl();
    @Mock
    GameHistoryRepository gameHistoryRepository;
    @Mock
    GameHistory gameHistory;
    @Mock
    TurnLogRepository turnLogRepository;

    Player firstPlayer = new Player(1L, "Rom-Cola", "666");
    Game game = new Game(firstPlayer, GameType.MILL);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testGetAllGamesHistoriesForCurrentPlayer() {
        List<GameHistory> list = new ArrayList<>();
        GameHistory gameHistory1 = new GameHistory(game);
        list.add(gameHistory1);

        when(gameHistoryRepository.findByPlayer(firstPlayer)).thenReturn(list);
        List<GameHistory> actualList = gameHistoryServiceImpl.getGameHistoriesForPlayer(firstPlayer);

        verify(gameHistoryRepository).findByPlayer(firstPlayer);
        assertThat(actualList, is(Arrays.asList(gameHistory1)));
    }


    @Test
    public void GetingGameHistoryById() throws Exception {
        when(gameHistoryRepository.findOne(1L)).thenReturn(gameHistory);
        GameHistory actualHistory = gameHistoryServiceImpl.getGameHistoryById(1L);

        verify(gameHistoryRepository).findOne(1L);
        assertThat(actualHistory, is(gameHistory));
    }

    @Test
    public void testNewGameHistoryCreationViaGame() {
        GameHistory gameHistory1 = new GameHistory(game);
        when(gameHistoryRepository.save(gameHistory1)).thenReturn(gameHistory1);

        gameHistoryServiceImpl.saveGameHistoryForGame(game);

        verify(gameHistoryRepository).save(gameHistory1);
        verify(turnLogRepository).save(game.getTurnList());
    }

    @Test
    public void testRemovingGameHistoriesForPlayer1() {
        List<GameHistory> histories = new ArrayList<>();

        gameHistoryServiceImpl.removeGameHistoriesForPlayer(firstPlayer);

        verify(gameHistoryRepository).findByPlayer(firstPlayer);
        verify(gameHistoryRepository).save(histories);
    }
}
