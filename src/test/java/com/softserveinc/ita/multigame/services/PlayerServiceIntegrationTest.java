package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.IntegrationTest;
import com.softserveinc.ita.multigame.model.Player;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@Category(IntegrationTest.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
public class PlayerServiceIntegrationTest {

    @Autowired
    PlayerService playerService;

    @Test
    public void playerCouldBeGetById() {
        Player p = playerService.get(101L);
        assertSame(101L, p.getId());
    }

    @Test
    public void nullCouldNotBeGet() {
        assertNull(playerService.get(null));
    }

    @Test
    public void playerCouldNotBeGetIfNotExist() {
        assertNull(playerService.get(1001L));
    }

    @Test
    public void playerCouldBeGetByLogin() {
        String login = "Vasya";
        Player p = playerService.getByLogin(login);
        assertSame(101L, p.getId());
        assertEquals(login, p.getLogin());
    }

    @Test
    public void nullCouldNotBeGetByLogin() {
        assertNull(playerService.getByLogin(null));
    }

    @Test
    public void allPlayersCouldBeGet() {
        assertTrue(playerService.getAll().size() > 0);
    }

    @Test
    public void playerCouldBeDeletedIfExist() {
        int beforeDel = playerService.getAll().size();
        Player p = playerService.get(105L);
        playerService.delete(p);
        int afterDel = playerService.getAll().size();
        assertSame(beforeDel - 1, afterDel);
    }

    @Test
    public void nullCouldNotBeDeleted() {
        int beforeDel = playerService.getAll().size();
        Player p = playerService.get(null);
        playerService.delete(p);
        int afterDel = playerService.getAll().size();
        assertSame(beforeDel, afterDel);
    }

    @Test
    public void playerCouldBeSaved() {
        int beforeSave = playerService.getAll().size();
        Player player = new Player("test", "test");
        playerService.saveOrUpdate(player);
        int afterSave = playerService.getAll().size();
        assertSame(beforeSave + 1, afterSave);
    }

    @Test
    public void playerWithSameLoginCouldNotBeSavedTwice() {
        Player player = new Player("test2", "test");
        playerService.saveOrUpdate(player);

        int beforeSave = playerService.getAll().size();
        player = new Player("test2", "test");
        playerService.saveOrUpdate(player);
        int afterSave = playerService.getAll().size();
        assertSame(beforeSave, afterSave);
    }

    @Test
    public void nullCouldNotBeSaved() {
        int beforeSave = playerService.getAll().size();
        assertNull(playerService.saveOrUpdate(null));
        int afterSave = playerService.getAll().size();
        assertSame(beforeSave, afterSave);
    }

    @Test
    public void playerCouldBeUpdateIfExist() {
        Player player = new Player("test3", "test");
        playerService.saveOrUpdate(player);

        player = playerService.getByLogin("test3");
        String fullName = "fullName";
        player.setFullName(fullName);
        player = playerService.saveOrUpdate(player);

        assertEquals(fullName, player.getFullName());
    }
}
