package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.IntegrationTest;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@Category(IntegrationTest.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
public class GameHistoryServiceIntegrationTest {

    @Autowired
    GameHistoryService gameHistoryService;
    @Autowired
    PlayerService playerService;

    private static Player p1;
    private static Player p2;

    @Before
    public void setUp() {
        playerService.saveOrUpdate(new Player("111", "111"));
        playerService.saveOrUpdate(new Player("222", "222"));

        p1 = playerService.getByLogin("111");
        p2 = playerService.getByLogin("222");
    }

    @Test
    public void gameHistoryCouldBeSavedByGame() {
        Game game = mock(Game.class);
        GameEngine engine = mock(GameEngine.class);

        when(game.getGameEngine()).thenReturn(engine);
        when(engine.getId()).thenReturn(200L);
        when(engine.getFirstPlayer()).thenReturn(p1);
        when(engine.getSecondPlayer()).thenReturn(p2);

        gameHistoryService.saveGameHistoryForGame(game);

        assertEquals((Long) 200L, (Long) gameHistoryService.getGameHistoryById(200L).getId());
        verify(engine).getFirstPlayer();
        verify(engine).getSecondPlayer();
    }

    @Test
    public void gameHistoriesCouldBeGetForPlayer() {
        Game game = mock(Game.class);
        GameEngine engine = mock(GameEngine.class);
        when(game.getGameEngine()).thenReturn(engine);
        when(engine.getId()).thenReturn(201L);
        when(engine.getFirstPlayer()).thenReturn(p1);
        when(engine.getSecondPlayer()).thenReturn(p2);

        gameHistoryService.saveGameHistoryForGame(game);

        assertTrue(gameHistoryService.getGameHistoriesForPlayer(p1).size() > 0);
        verify(engine).getFirstPlayer();
        verify(engine).getSecondPlayer();
    }


    @Test
    public void gameHistoriesCouldBeNotGetIfPlayerNull() {
        assertTrue(gameHistoryService.getGameHistoriesForPlayer(null).size() == 0);
    }

    @Test
    public void gameHistoriesCouldBeRemoveForPlayer() {
        Game game = mock(Game.class);
        GameEngine engine = mock(GameEngine.class);
        when(game.getGameEngine()).thenReturn(engine);
        when(engine.getId()).thenReturn(202L);
        when(engine.getFirstPlayer()).thenReturn(p1);
        when(engine.getSecondPlayer()).thenReturn(p2);

        gameHistoryService.saveGameHistoryForGame(game);

        assertTrue(gameHistoryService.getGameHistoriesForPlayer(p1).size() > 0);
        gameHistoryService.removeGameHistoriesForPlayer(p1);
        assertTrue(gameHistoryService.getGameHistoriesForPlayer(p1).size() == 0);
    }

    @Test
    public void gameHistoryServiceCouldGetTheWinnerAndLoser() {
        Game game = mock(Game.class);
        GameEngine engine = mock(GameEngine.class);
        when(game.getGameEngine()).thenReturn(engine);
        when(engine.getId()).thenReturn(203L);
        when(engine.getFirstPlayer()).thenReturn(p1);
        when(engine.getSecondPlayer()).thenReturn(p2);
        when(engine.getGameState()).thenReturn(GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER);


        gameHistoryService.saveGameHistoryForGame(game);
        assertEquals(p1, gameHistoryService.getWinner(203L));
        assertEquals(p2, gameHistoryService.getLoser(203L));


        when(game.getGameEngine()).thenReturn(engine);
        when(engine.getId()).thenReturn(204L);
        when(engine.getFirstPlayer()).thenReturn(p1);
        when(engine.getSecondPlayer()).thenReturn(p2);
        when(engine.getGameState()).thenReturn(GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER);

        gameHistoryService.saveGameHistoryForGame(game);
        assertEquals(p2, gameHistoryService.getWinner(204L));
        assertEquals(p1, gameHistoryService.getLoser(204L));
    }

}
