package com.softserveinc.ita.multigame.model.engine;

import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class GenericGameEngineTest {

    private Player thePlayer;
    private Player theSamePlayer;
    private Player anotherPlayer;

    private GenericGameEngine engine;


    @Before
    public void setUp() {
        engine = new EmptyGameEngine();

        thePlayer = new Player();

        thePlayer.setId(1L);
        thePlayer.setLogin("111");

        theSamePlayer = thePlayer;

        anotherPlayer = new Player();
        anotherPlayer.setId(2L);
        anotherPlayer.setLogin("222");
    }

    @Test
    public void PlayerCouldBeSetIfNullOrIdIsNullOrLoginIsNull() {
        assertFalse(engine.setFirstPlayer(null));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_PLAYER);

        Player player = new Player();
        player.setLogin("111");
        assertFalse(engine.setFirstPlayer(player));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_PLAYER);

        player = new Player();
        player.setId(1L);
        assertFalse(engine.setFirstPlayer(player));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_PLAYER);
    }

    @Test
    public void firstPlayerCouldBeSetAsAFirstOne() {
        assertTrue(engine.setFirstPlayer(thePlayer));
    }

    @Test
    public void notValidPlayerCouldNotBeSetAsAFirstOne() {
        assertFalse(engine.setFirstPlayer(new Player()));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_PLAYER);
    }

    @Test
    public void notValidPlayerCouldNotBeSetAsASecondtOne() {
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertFalse(engine.setSecondPlayer(new Player()));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_PLAYER);
    }

    @Test
    public void firstPlayerCouldNotBeSetAsASecondOne() {
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertFalse(engine.setSecondPlayer(theSamePlayer));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_FIRST_PLAYER_ORDER);
    }

    @Test
    public void theFirstPlayerCouldNotBeSetTwice() {
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertFalse(engine.setFirstPlayer(theSamePlayer));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_FIRST_PLAYER_ORDER);
    }

    @Test
    public void theSecondPlayerCouldNotBeSetIfFirstNotSet() {
        assertFalse(engine.setSecondPlayer(thePlayer));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_SECOND_PLAYER_ORDER);
    }

    @Test
    public void theSecondPlayerCouldNotBeSetTwice() {
        assertTrue(engine.setFirstPlayer(anotherPlayer));
        assertTrue(engine.setSecondPlayer(thePlayer));
        assertFalse(engine.setFirstPlayer(theSamePlayer));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_FIRST_PLAYER_ORDER);
    }

    @Test
    public void twoPlayersCouldBeSetOneByOne() {
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertTrue(engine.setSecondPlayer(anotherPlayer));
        assertTrue(engine.getResultCode() == GameResultCode.OK);
    }

    @Test
    public void afterTheSecondPlayerIsSetTheGameShouldBeStarted() {
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertTrue(engine.setSecondPlayer(anotherPlayer));
        assertTrue(engine.isStarted());
    }

    @Test
    public void createdGameShouldNotBeFinished() {
        assertFalse(engine.isFinished());
    }


    @Test
    public void firstAndSecondAndThridGameEnginesSouldHaveIDs0and1and2() {
        setGameEngineNextId(0L);

        GameEngine firstEngine = new EmptyGameEngine();
        GameEngine secondEngine = new EmptyGameEngine();
        GameEngine thirdEngine = new EmptyGameEngine();

        assertEquals((Long) 0L, (Long) firstEngine.getId());
        assertEquals((Long) 1L, (Long) secondEngine.getId());
        assertEquals((Long) 2L, (Long) thirdEngine.getId());

        assertNotEquals(firstEngine, secondEngine);
        assertNotEquals(firstEngine, thirdEngine);
    }

    @Test(expected = RuntimeException.class)
    public void whenMaxGameIdValueIsReachedExceptionShouldBeThrown() {
        setGameEngineNextId(Long.MAX_VALUE - 1);
        new EmptyGameEngine();
    }

    @Test
    public void couldNotMakeTurnIfSecondPlayerNotSet() {
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertFalse(engine.makeTurn(thePlayer, "1"));
        assertTrue(engine.getResultCode() == GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME);

    }

    @Test
    public void couldNotMakeTurnIfGameIsFinished() {
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertTrue(engine.setSecondPlayer(anotherPlayer));
        engine.gameState = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        assertFalse(engine.makeTurn(anotherPlayer, "1"));
        assertSame(engine.getResultCode(), GameResultCode.BAD_TURN_FOR_FINISHED_GAME);
    }

    @Test
    public void gameEngineCouldDefineWinner() {
        engine = new FinishedWithFirstPlayerGameEngine();
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertTrue(engine.setSecondPlayer(anotherPlayer));
        assertTrue(engine.makeTurn(thePlayer, "1"));
        assertSame(thePlayer, engine.getTheWinner());

        engine = new FinishedWithSecondPlayerGameEngine();
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertTrue(engine.setSecondPlayer(anotherPlayer));
        assertTrue(engine.makeTurn(thePlayer, "1"));
        assertSame(anotherPlayer, engine.getTheWinner());

        engine = new FinishedWithDrawGameEngine();
        assertTrue(engine.setFirstPlayer(thePlayer));
        assertTrue(engine.setSecondPlayer(anotherPlayer));
        assertTrue(engine.makeTurn(thePlayer, "1"));
        assertNull(engine.getTheWinner());
    }


    private class EmptyGameEngine extends GenericGameEngine {
        @Override
        protected boolean validateTurnLogic(String turn) {
            return true;
        }

        @Override
        protected GameState changeGameState(Player player, String turn) {
            return GameState.UNDEFINED;
        }
    }

    private class FinishedWithFirstPlayerGameEngine extends GenericGameEngine {
        @Override
        protected boolean validateTurnLogic(String turn) {
            return true;
        }

        @Override
        protected GameState changeGameState(Player player, String turn) {
            return GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        }
    }

    private class FinishedWithSecondPlayerGameEngine extends GenericGameEngine {
        @Override
        protected boolean validateTurnLogic(String turn) {
            return true;
        }

        @Override
        protected GameState changeGameState(Player player, String turn) {
            return GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        }
    }

    private class FinishedWithDrawGameEngine extends GenericGameEngine {
        @Override
        protected boolean validateTurnLogic(String turn) {
            return true;
        }

        @Override
        protected GameState changeGameState(Player player, String turn) {
            return GameState.FINISHED_WITH_DRAW;
        }
    }

    private static void setGameEngineNextId(Long newValue) {
        try {
            Field field = GenericGameEngine.class.getDeclaredField("nextId");
            field.setAccessible(true);
            field.set(null, newValue);
        } catch (Exception e) {
            fail("Field \"nextId\" is not accessible");
        }
    }


}
