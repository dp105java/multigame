package com.softserveinc.ita.multigame.model.engine.rsp;


import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;

import static com.softserveinc.ita.multigame.model.engine.rsp.RockScissorsPaper.happensDraw;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TestHappensDraw {

    Player player1;
    Player player2;
    RockScissorsPaper rockScissorsPaper;

    @Before
    public void setUp() {

        player1 = new Player(1L, "Vasya", "1111");
        player2 = new Player(2L, "Petya", "1111");
        rockScissorsPaper = new RockScissorsPaper(1L);

    }

    @Test
    public void testHappensDrawByTheRules() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "PAPER");
        rockScissorsPaper.makeTurn(player2, "PAPER");

        assertTrue(happensDraw());
    }

    @Test
    public void testHappensDrawByTheRulesWrong() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "SPOCK");
        rockScissorsPaper.makeTurn(player2, "LIZARD");

        assertFalse(happensDraw());
    }

    @Test(expected = NullPointerException.class)
    public void testHappensDrawByTheRulesWrongWithNullThrowsNullPointerEx() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, null);
        rockScissorsPaper.makeTurn(player2, null);

        assertTrue(happensDraw());
    }
}
