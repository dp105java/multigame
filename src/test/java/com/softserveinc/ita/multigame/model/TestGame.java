package com.softserveinc.ita.multigame.model;

import com.softserveinc.ita.multigame.model.engine.halma.HalmaGameEngine;
import com.softserveinc.ita.multigame.model.engine.mill.MillGameEngine;
import com.softserveinc.ita.multigame.model.engine.renju.RenjuGame;
import com.softserveinc.ita.multigame.model.engine.reversi.Reversi;
import com.softserveinc.ita.multigame.model.engine.rsp.RockScissorsPaper;
import com.softserveinc.ita.multigame.model.engine.seabattle.SeaBattle;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestGame {

    private Game game;

    @Mock
    private Player player;
    @Mock
    private Player player2;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(player.getLogin()).thenReturn("Yoda");
        when(player2.getLogin()).thenReturn("Dart_Maul");
    }

    @Test
    public void checkReversiGameCreation() {
        game = new Game(player, GameType.REVERSI);

        assertThat(game.getGameEngine(), instanceOf(Reversi.class));
        assertThat(game.getCreationTime(), notNullValue());
    }

    @Test
    public void checkHalmaGameCreation() {
        game = new Game(player, GameType.HALMA);

        assertThat(game.getGameEngine(), instanceOf(HalmaGameEngine.class));
        assertThat(game.getCreationTime(), notNullValue());
    }

    @Test
    public void checkRenjuGameCreation() {
        game = new Game(player, GameType.RENJU);

        assertThat(game.getGameEngine(), instanceOf(RenjuGame.class));
        assertThat(game.getCreationTime(), notNullValue());
    }

    @Test
    public void checkMillGameCreation() {
        game = new Game(player, GameType.MILL);

        assertThat(game.getGameEngine(), instanceOf(MillGameEngine.class));
        assertThat(game.getCreationTime(), notNullValue());
    }

    @Test
    public void checkRSPGameCreation() {
        game = new Game(player, GameType.RSP);

        assertThat(game.getGameEngine(), instanceOf(RockScissorsPaper.class));
        assertThat(game.getCreationTime(), notNullValue());
    }

    @Test
    public void checkSeaBattleGameCreation() {
        game = new Game(player, GameType.SEABATTLE);

        assertThat(game.getGameEngine(), instanceOf(SeaBattle.class));
        assertThat(game.getCreationTime(), notNullValue());
    }

    @Test
    public void checkAddTurnToTurnsListViaMakeTurnMethod() {
        game = new Game(player, GameType.REVERSI);
        assertEquals(0, game.getTurnList().size());
        game.getGameEngine().setSecondPlayer(player2);
        game.makeTurn(player, "f4");
        assertEquals(1, game.getTurnList().size());
    }

    @Test
    public void checkThatMakeTurnMethodDoesNotAddTurnToTurnListIfTurnIsWrong() {
        game = new Game(player, GameType.REVERSI);
        assertEquals(0, game.getTurnList().size());
        game.getGameEngine().setSecondPlayer(player2);
        game.makeTurn(player, "e5");
        assertEquals(0, game.getTurnList().size());
    }
}
