package com.softserveinc.ita.multigame.model;

import com.softserveinc.ita.multigame.services.PlayerService;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGameListManagerImpl {

    GameListManagerImpl gameListManager;
    Player player1;
    Player player2;

    @Before
    public void setUp() {
        gameListManager = new GameListManagerImpl();

        player1 = mock(Player.class);
        player2 = mock(Player.class);
        when(player1.getId()).thenReturn(1L);
        when(player1.getLogin()).thenReturn("Ivan");
        when(player2.getId()).thenReturn(2L);
        when(player2.getLogin()).thenReturn("Alex");
        gameListManager.playerService = mock(PlayerService.class);

        when(gameListManager.playerService.get(1L)).thenReturn(player1);
        when(gameListManager.playerService.get(2L)).thenReturn(player2);

    }

    @Test
    public void getAllCreatedGameIdsForCreator() {
        gameListManager.createGame(player1, GameType.SEABATTLE);
        List<Long> ids = gameListManager.getAllIds();
        assertEquals(gameListManager.getCreatedGamesIds(player1), Arrays.asList(ids.get(0)));
    }

    @Test
    public void getZeroCreatedGameIdsForPlayerWhoIsNotInvolvedInGameProcess() {
        gameListManager.createGame(player1, GameType.SEABATTLE);

        assertEquals(gameListManager.getCreatedGamesIds(player2), Arrays.asList());
    }

    @Test
    public void getZeroWaitingGameIdsForPlayerWhoCreatedAllGames() {
        gameListManager.createGame(player1, GameType.SEABATTLE);
        gameListManager.createGame(player1, GameType.SEABATTLE);

        assertEquals(gameListManager.getWaitingGamesIds(player1), Arrays.asList());
    }

    @Test
    public void getAllWaitingGameIdsForPlayerWhoIsNotInvolvedInGameProcess() {
        gameListManager.createGame(player1, GameType.SEABATTLE);

        assertEquals(gameListManager.getWaitingGamesIds(player2), gameListManager.getAllIds());
    }

    @Test
    public void getZeroPlayingGameIdsForPlayerWhoIsNotInvolvedInGameProcess() {
        gameListManager.createGame(player1, GameType.SEABATTLE);
        gameListManager.createGame(player1, GameType.SEABATTLE);
        gameListManager.createGame(player1, GameType.SEABATTLE);

        assertEquals(gameListManager.getPlayingGamesIds(player2), Arrays.asList());
    }

    @Test
    public void gePlayingGameIdsForCreator() {
        gameListManager.createGame(player1, GameType.SEABATTLE);

        List<Long> ids = gameListManager.getAllIds();

        gameListManager.getGame(ids.get(0)).getGameEngine().setSecondPlayer(player2);
        assertEquals(gameListManager.getPlayingGamesIds(player1), Arrays.asList(ids.get(0)));
    }

    @Test
    public void gePlayingGameIdsForSecondPlayer() {
        gameListManager.createGame(player1, GameType.SEABATTLE);

        List<Long> ids = gameListManager.getAllIds();

        gameListManager.getGame(ids.get(0)).getGameEngine().setSecondPlayer(player2);
        assertEquals(gameListManager.getPlayingGamesIds(player2), Arrays.asList(ids.get(0)));
    }

    @Test
    public void deletingAllGamesClearsGameList() {
        gameListManager.createGame(player1, GameType.SEABATTLE);
        List<Long> ids = gameListManager.getAllIds();

        gameListManager.deleteGame(ids.get(0));
        assertEquals(gameListManager.getAllIds(), Arrays.asList());
    }

}
