package com.softserveinc.ita.multigame.model.engine.rsp;


import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;

import static com.softserveinc.ita.multigame.model.engine.rsp.RockScissorsPaper.firstPlayerWins;
import static com.softserveinc.ita.multigame.model.engine.rsp.RockScissorsPaper.happensDraw;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TestFirstPlayerWins {

    Player player1;
    Player player2;
    RockScissorsPaper rockScissorsPaper;

    @Before
    public void setUp() {

        player1 = new Player(1L, "Vasya", "1111");
        player2 = new Player(2L, "Petya", "1111");
        rockScissorsPaper = new RockScissorsPaper(1L);
    }

    @Test
    public void testFirstPlayerWinsByTheRules() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "ROCK");
        rockScissorsPaper.makeTurn(player2, "SCISSORS");

        assertTrue(firstPlayerWins());
    }

    @Test
    public void testFirstPlayerLosesByTheRules() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "PAPER");
        rockScissorsPaper.makeTurn(player2, "SCISSORS");

        assertFalse(firstPlayerWins());
    }

    @Test
    public void testFirstPlayerDrawsByTheRules() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "SPOCK");
        rockScissorsPaper.makeTurn(player2, "SPOCK");

        assertTrue(happensDraw());
    }

    @Test(expected = NullPointerException.class)
    public void testFirstPlayerFailsIfTurnIsNullAndTrowsNullPointerEx() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, null);
        rockScissorsPaper.makeTurn(player2, "SPOCK");

        assertFalse(firstPlayerWins());
    }

}

