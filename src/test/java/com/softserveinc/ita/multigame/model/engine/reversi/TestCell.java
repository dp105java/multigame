package com.softserveinc.ita.multigame.model.engine.reversi;

        import org.junit.Test;

        import static org.junit.Assert.assertEquals;

public class TestCell {

    private Cell cell;

    @Test
    public void checkGetRightToStringResponse() {
        cell = new Cell(3, 4, 1);
        String expectedValue = "{\"x\": " + 3 + ", \"y\": " + 4 + ", \"value\": " + 1 + "}";

        assertEquals(expectedValue, cell.toString());
    }
}
