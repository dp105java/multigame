package com.softserveinc.ita.multigame.model.engine.seabattle;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.model.engine.GameResultCode;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

public class TestSeaBattle {
    GameEngine game;
    Player player1;
    Player player2;

    @Before
    public void setUp(){
        game = new SeaBattle();
        player1 = new Player(1L, "Ivan", "1");
        player2 = new Player(2L, "Petro", "2");
        game.setFirstPlayer(player1);
        game.setSecondPlayer(player2);
    }
    @Test
    public void playerHitWithResultCodeOk(){
        GameResultCode expected = GameResultCode.OK;

        game.makeTurn(game.getFirstPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }
    @Test
    public void playerCannotTurnTwiceIfMiss(){
        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;

        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getFirstPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }
    @Test
    public void playerMustTurnTwiceIfHit(){
        GameResultCode expected = GameResultCode.OK;

        game.makeTurn(game.getFirstPlayer(), "a1");
        game.makeTurn(game.getFirstPlayer(), "a2");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void cannotStartWithoutBothPlayersRegistration(){
        GameResultCode expected = GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME;

        GameEngine game = new SeaBattle();
        game.setFirstPlayer(player1);
        game.makeTurn(game.getFirstPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }
    @Test
    public void returnTurnToPlayerIfHeHitSamePlace(){

        GameResultCode expected = GameResultCode.OK;

        game.makeTurn(game.getFirstPlayer(), "a1");
        game.makeTurn(game.getFirstPlayer(), "a1");
        game.makeTurn(game.getFirstPlayer(), "a2");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void returnTurnToSecondPlayerIfHeHitSamePlace(){

        GameResultCode expected = GameResultCode.OK;

        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getSecondPlayer(), "a1");
        game.makeTurn(game.getSecondPlayer(), "a1");
        game.makeTurn(game.getSecondPlayer(), "b1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void dontReturnTurnToAnotherPlayerIfPreviousHitSamePlace(){

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;

        game.makeTurn(game.getFirstPlayer(), "a1");
        game.makeTurn(game.getFirstPlayer(), "a1");
        game.makeTurn(game.getSecondPlayer(), "a2");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void dontReturnTurnToFirstPlayerIfSecondHitSamePlace(){

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;

        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getSecondPlayer(), "a1");
        game.makeTurn(game.getSecondPlayer(), "a1");
        game.makeTurn(game.getFirstPlayer(), "a2");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void returnTurnToPlayerIfHeMissedSamePlace(){

        GameResultCode expected = GameResultCode.OK;

        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getSecondPlayer(), "j10");
        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getFirstPlayer(), "a2");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void returnTurnToSecondPlayerIfHeMissedSamePlace(){

        GameResultCode expected = GameResultCode.OK;

        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getSecondPlayer(), "j10");
        game.makeTurn(game.getFirstPlayer(), "j10");
        game.makeTurn(game.getSecondPlayer(), "j10");
        game.makeTurn(game.getSecondPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void dontReturnTurnToAnotherPlayerIfPreviousMissedSamePlace(){

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;

        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getSecondPlayer(), "j10");
        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getSecondPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void dontReturnTurnToFirstPlayerIfSecondMissedSamePlace(){

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;

        game.makeTurn(game.getFirstPlayer(), "a5");
        game.makeTurn(game.getSecondPlayer(), "j10");
        game.makeTurn(game.getFirstPlayer(), "j9");
        game.makeTurn(game.getSecondPlayer(), "j10");
        game.makeTurn(game.getFirstPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void cannotStartIfOnlySecondPlayerRegistrated(){

        GameResultCode expected = GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME;

        GameEngine game = new SeaBattle();
        game.setSecondPlayer(player2);
        game.makeTurn(game.getSecondPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void cannotStartIfSecondPlayerRegistratedBeforeFirstPlayer(){

        GameResultCode expected = GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME;

        GameEngine game = new SeaBattle();
        game.setSecondPlayer(player2);
        game.setFirstPlayer(player1);
        game.makeTurn(game.getFirstPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void cannotStartWithNullFirstPlayer(){

        GameResultCode expected = GameResultCode.BAD_PLAYER;

        GameEngine game = new SeaBattle();
        game.setFirstPlayer(null);
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }


    @Test
    public void cannotStartWithNullSecondPlayerName(){

        GameResultCode expected = GameResultCode.BAD_PLAYER;

        GameEngine game = new SeaBattle();
        game.setFirstPlayer(player1);
        game.setSecondPlayer(null);
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void showPlayerIfHeHitSamePlace(){

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;

        game.makeTurn(game.getFirstPlayer(), "a1");
        game.makeTurn(game.getFirstPlayer(), "a1");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void showPlayerIfHeMissedSamePlace(){

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;

        game.makeTurn(game.getFirstPlayer(), "j10");
        game.makeTurn(game.getSecondPlayer(), "j10");
        game.makeTurn(game.getFirstPlayer(), "J10");
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void cannotSetNameAfterStart(){

        GameResultCode expected = GameResultCode.BAD_FIRST_PLAYER_ORDER;

        game.setFirstPlayer(player2);
        GameResultCode actual = game.getResultCode();

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void defaultFieldsAreNotEquals(){
        assertNotEquals(game.getBoard(player1), game.getBoard(player2));
    }

    @Test
    public void secondPlayerCanSetField(){
        GameResultCode expected = GameResultCode.OK;

        game.setField(player2, (List<Cell>) game.getBoard(player1));

        GameResultCode actual = game.getResultCode();
        assertThat(actual, equalTo(expected));
    }
}
