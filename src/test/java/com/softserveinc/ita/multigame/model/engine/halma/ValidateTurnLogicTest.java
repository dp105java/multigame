package com.softserveinc.ita.multigame.model.engine.halma;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ValidateTurnLogicTest extends HalmaGameEngineTest {

    //Validate logic
    @Test
    public void checkIfTurnToBusyCell() {
        String turn = "B6,B5";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void checkIfTurnToBusyCell2() {
        String turn = "A1,A2";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void firstPlayerTryToMoveSecondPlayerFigure() {
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        String turn = "P12,P11";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void secondPlayerTryToMoveFirstPlayerFigure() {
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        String turn = "E1,E2";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void tryToJumpDownOverEmptyCell() {
        String turn = "E3,E5";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void tryToJumpUpOverEmptyCell() {
        String turn = "M14,M12";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void tryToJumpRightOverEmptyCell() {
        String turn = "B5,E5";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void tryToJumpLeftOverEmptyCell() {
        String turn = "O12,O10";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void tryToTurnFromEmptyCellByFirstPlayer() {
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        String turn = "A6,A7";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

    @Test
    public void tryToTurnFromEmptyCellBySecondPlayer() {
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        String turn = "A6,A7";

        assertThat(halmaGameEngine.validateTurnLogic(turn), is(false));
    }

}
