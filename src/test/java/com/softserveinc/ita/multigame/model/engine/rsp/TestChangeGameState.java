package com.softserveinc.ita.multigame.model.engine.rsp;


import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TestChangeGameState {

    Player player1;
    Player player2;
    RockScissorsPaper rockScissorsPaper;

    @Before
    public void setUp() {
        player1 = new Player(1L, "Vasya", "1111");
        player2 = new Player(2L, "Petya", "1111");
        rockScissorsPaper = new RockScissorsPaper(1L);
    }

    @Test
    public void testChangeStateWnenStateIsFirstPlayerWin() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "ROCK");
        rockScissorsPaper.makeTurn(player2, "SCISSORS");

        GameState expected = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        GameState actual = rockScissorsPaper.changeGameState(player1, "ROCK");

        assertEquals(expected, actual);
    }

    @Test
    public void testChangeStateWnenStateIsSecondPlayerWin() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "ROCK");
        rockScissorsPaper.makeTurn(player2, "PAPER");

        GameState expected = GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        GameState actual = rockScissorsPaper.changeGameState(player1, "ROCK");

        assertEquals(expected, actual);
    }

    @Test
    public void testChangeStateWnenStateIsDraw() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "ROCK");
        rockScissorsPaper.makeTurn(player2, "ROCK");

        GameState expected = GameState.FINISHED_WITH_DRAW;
        GameState actual = rockScissorsPaper.changeGameState(player2, "ROCK");

        assertEquals(expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void testWhenTurnIsNullTrowsNullPointerException() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, null);
        rockScissorsPaper.makeTurn(player2, "ROCK");

        GameState expected = GameState.FINISHED_WITH_DRAW;
        GameState actual = rockScissorsPaper.changeGameState(player2, "ROCK");

        assertEquals(expected, actual);
    }
}
