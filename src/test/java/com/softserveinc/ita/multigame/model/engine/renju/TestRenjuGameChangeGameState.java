package com.softserveinc.ita.multigame.model.engine.renju;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRenjuGameChangeGameState {

    RenjuGame game = new RenjuGame();

    @Before
    public void setUp() {
        Player petro = new Player("Petro", "petro");
        petro.setId(1L);
        Player mykola = new Player("Mykola", "mykola");
        mykola.setId(2L);
        game.setFirstPlayer(petro);
        game.setSecondPlayer(mykola);
    }

    @Test
    public void secondPlayerTurn() {
        assertEquals(game.changeGameState(game.getFirstPlayer(), "3,3"),
                GameState.WAIT_FOR_SECOND_PLAYER_TURN);
    }

    @Test
    public void firstPlayerTurn() {
        game.makeTurn(game.getFirstPlayer(), "2,2");
        assertEquals(game.changeGameState(game.getSecondPlayer(), "4,8"),
                GameState.WAIT_FOR_FIRST_PLAYER_TURN);
    }

    @Test
    public void secondPlayerTurnAfterWrongTurn() {
        game.makeTurn(game.getFirstPlayer(), "2,2");
        assertEquals(game.changeGameState(game.getFirstPlayer(), "4,8"),
                GameState.WAIT_FOR_SECOND_PLAYER_TURN);
        assertEquals(game.changeGameState(game.getFirstPlayer(), "7,7"),
                GameState.WAIT_FOR_SECOND_PLAYER_TURN);
    }

    @Test
    public void firstPlayerTurnAfterWrongTurn() {
        game.makeTurn(game.getFirstPlayer(), "2,2");
        game.makeTurn(game.getSecondPlayer(), "4,4");
        assertEquals(game.changeGameState(game.getSecondPlayer(), "4,5"),
                GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        assertEquals(game.changeGameState(game.getSecondPlayer(), "4,6"),
                GameState.WAIT_FOR_FIRST_PLAYER_TURN);
    }

    @Test
    public void firstPlayerWin() {
        game.makeTurn(game.getFirstPlayer(), "2,2");
        game.makeTurn(game.getSecondPlayer(), "4,4");
        game.makeTurn(game.getFirstPlayer(), "2,3");
        game.makeTurn(game.getSecondPlayer(), "5,5");
        game.makeTurn(game.getFirstPlayer(), "2,1");
        game.makeTurn(game.getSecondPlayer(), "6,6");
        game.makeTurn(game.getFirstPlayer(), "2,0");
        game.makeTurn(game.getSecondPlayer(), "11,0");
        game.makeTurn(game.getFirstPlayer(), "2,4");
        assertEquals(game.getFirstPlayer(), game.getTheWinner());
    }

    @Test
    public void secondPlayerWin() {
        game.makeTurn(game.getFirstPlayer(), "2,2");
        game.makeTurn(game.getSecondPlayer(), "4,4");
        game.makeTurn(game.getFirstPlayer(), "2,13");
        game.makeTurn(game.getSecondPlayer(), "5,5");
        game.makeTurn(game.getFirstPlayer(), "7,1");
        game.makeTurn(game.getSecondPlayer(), "6,6");
        game.makeTurn(game.getFirstPlayer(), "2,0");
        game.makeTurn(game.getSecondPlayer(), "8,8");
        assertEquals(game.getSecondPlayer(), game.getTheWinner());
    }

}
