'use strict';

function refreshField() {

    $.ajax({
        type: "GET",
        url: 'ajax/board',
        data: 'gameId=' + $('#idValue').val(),
        dataType: 'text',
        success: function (responseText) {
            displayTips();
            clearField();
            drawField();
            syncField(responseText);
            refreshResultCode();
        },
        error: function (responseText) {
            console.log("bad connection to server");
        }
    });

}
