'use strict';

// FIELD

var drawCell = function (x, y, color) {
    $('#field').append('<div id="' + defineId(x, y) + '" class="' + color + 'cell" onclick="makeTurn(this.id)"></div>');
    $('#' + defineId(x, y)).css({top: y * 50, left: x * 50});
};

var drawField = function () {
    for (var y = 0; y < 8; y++) {
        drawRow(y);
    }
};

var drawRow = function (y) {
    for (var x = 0; x < 8; x++) {
        drawCell(x, y, defineColor(x, y));
    }
};

var defineId = function (x, y) {
    return "cell_" + (x * 50) + "_" + (y * 50);
};

var defineColor = function (x, y) {
    return (x + y) % 2 === 0 ? 'white' : 'black';
};

// PAWN

var drawPawn = function (x, y, color) {
    $('#' + defineId(x, y)).append('<div class="' + color + 'pawn"></div>');
};

var definePawnColor = function (pawnIdx) {
    return pawnIdx === 1 ? 'white' : 'black';
};

// TRANSLATOR

var syncField = function (matrix) {
    if (matrix == null) {
        return;
    }

    matrix = JSON.parse(matrix);
    $.each(matrix, function (i, pawn) {
        if (pawn.value) {
            drawPawn(pawn.x, pawn.y, definePawnColor(pawn.value));
        }
    });
};

function clearField() {
    document.getElementById('field').innerHTML = '';
}
