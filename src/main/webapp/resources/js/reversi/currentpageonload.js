'use strict';

$(document).ready(function () {
    drawField();
    refreshField();
    displayTips();
    refreshResultCode();
    setInterval(function () {
        refreshField()
    }, 2000);
});
