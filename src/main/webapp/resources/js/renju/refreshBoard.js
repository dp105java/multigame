$(document).ready(function () {
    refreshPlayerTurn();
    refreshBoard();
    setInterval(function () {
        refreshPlayerTurn();
        refreshBoard();
    }, 3000); //3 seconds
});

function refreshBoard() {
    $.ajax({
        method: 'GET',
        url: 'ajax/board',
        data: 'gameId=' + $('#gameId').val(),
        success: function (board) {
            if (board == null) {
                return;
            }
            $('#board').html('<table>');
            $('#board').append('<thead>');
            $('#board').append('<tr>');
            $('#board').append('<th>#</th>');
            $.each(board, function (i, elem) {
                $('#board').append('<th>' + i + '</th>');
            });
            $('#board').append('</tr>');
            $('#board').append('</thead>');
            $('#board').append('<tbody>');
            $.each(board, function (i, elem) {
                $('#board').append('<tr>');
                $('#board').append('<td width="32" style="vertical-align:middle;"><b>' + i + '</b></td>');
                $.each(elem, function (j, inner) {
                    if (inner == "B") {
                        $('#board').append('<td width="32" height="32" style="border: 1px solid grey; vertical-align:middle;">'
                            + '<img width="30" height="30" src="resources/img/renju/black_stone.jpg">'
                            + '</td>');
                    } else if (inner == "W") {
                        $('#board').append('<td width="32" height="32" style="border: 1px solid grey; vertical-align:middle;">'
                            + '<img width="30" height="30" src="resources/img/renju/white_stone.jpg">'
                            + '</td>');
                    } else {
                        $('#board').append('<td width="32" height="32" style="border: 1px solid grey; vertical-align:middle;">'
                            + inner + '</td>');
                    }
                });
                $('#board').append('</tr>');
            });
            $('#board').append('</tbody>');
            $('#board').append('</table>');

        },
        error: function (errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
}

function refreshPlayerTurn() {
    $
        .ajax({
            method: 'GET',
            url: 'ajax/tips',
            data: 'gameId=' + $('#gameId').val(),
            success: function (data) {
                if (data.message == 'GAME OVER') {
                    window.location.replace('finish?gameId=' + $('#gameId').val());
                }
                $('#tips').html('<h2 style="color:red">' + data.message + '</h2>');
            },
            error: function (errorThrown) {
                alert("Error: " + errorThrown);
            }
        });
}