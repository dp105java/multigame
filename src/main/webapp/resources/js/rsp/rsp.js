$(document).ready(function () {
    $('.image').click(function () {
        var turn = $(this).attr('id');
        var gameId = $('#gameId').attr('value');
        console.log(gameId);
        console.log(turn);
        refreshPlayerTurn();
        makeTurnClick(gameId, turn);
    });
    setInterval(function () {
        displayTips();
        refreshPlayerTurn();
    }, 2000); //3 seconds
});

function makeTurnClick(g, t) {
    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: g, turn: t},
        success: function (data) {
            console.log("success")
        },
        error: function () {
            console.log('Error with ajax/makeTurn');
            console.log(g);
            console.log(t);
        }
    });
}

function refreshPlayerTurn() {
    $
        .ajax({
            method: 'GET',
            url: 'rsp/refreshTurn',
            data: 'gameId=' + $('#gameId').val(),
            success: function (data) {
                $('#playerTurn').html(
                    '<h2 style="color:red">'
                    + data.message
                    + '</h2>');
            },
            error: function (errorThrown) {
                console.log("Error with /rsp/refreshTurn: " + errorThrown);
            }
        });
}

function displayTips() {
    $.ajax({
        method: 'GET',
        url: 'ajax/tips?gameId=' + $('#gameId').val(),
        success: function (tips) {
            if (tips.message == 'GAME OVER') {
                window.location.replace('finish?gameId=' + $('#gameId').val());
            }
            $('#tips').html(tips.message)
        }
    });
}
