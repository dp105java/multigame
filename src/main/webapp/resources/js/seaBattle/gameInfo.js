$(document).ready(function () {
    showBoard1();
    refreshBoard2();
    setInterval(function () {
        showBoard1()
    }, 3000);
});
function showBoard1() {
    $.ajax({
        method: 'GET',
        url: 'seabattle/show?id=' + $('#idOfGame').val(),
        success: function (data1) {
            $.each(data1, function (index, element) {
                if (element.address == 'TheEnd') {
                    document.location.href = 'finish?gameId=' + $('#idOfGame').val();
                }
            });
            $('#board1').empty();
            $('#board1').append('<img class="target" src="resources/img/seaBattle/target.png" width="40" height="40"/>' +
                '<span class="digit">1</span>' +
                '<span class="digit">2</span> ' +
                '<span class="digit">3</span>' +
                '<span class="digit">4</span> ' +
                '<span class="digit">5</span> ' +
                '<span class="digit">6</span> ' +
                '<span class="digit">7</span> ' +
                '<span class="digit">8</span> ' +
                '<span class="digit">9</span> ' +
                '<span class="digit">10</span><br/>');
            var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

            $.each(data1, function (index, element) {
                if (index % 10 == 0) {
                    var l = letters[index / 10];
                    $('#board1').append('<span class="letter" style="font-size: 26px;">' + l + '</span>');
                }
                switch (element.value) {
                    case '#':
                        $('#board1').append('<img class="celly" src="resources/img/seaBattle/ship3.png" width="40" height="40"/>');
                        break;
                    case '_':
                        $('#board1').append('<img class="celly" src="resources/img/seaBattle/anchor2.png" width="40" height="40"/>');
                        break;
                    case 'X':
                        $('#board1').append('<img class="celly" src="resources/img/seaBattle/destroyed4.png" width="40" height="40"/>');
                        break;
                    case '*':
                        $('#board1').append('<img class="celly" src="resources/img/seaBattle/miss.png" width="40" height="40"/>');
                        break;
                }
                for (var x = 0; x < 10; x++) {
                    if (index == (9 + 10 * x)) {
                        $('#board1').append('<br/>');
                    }
                }
            });

        },
        error: function () {
            alert('Error while printing Board1!');
        }
    });
}
function refreshBoard2() {
    $.ajax({
        method: 'GET',
        url: 'seabattle/show2?id=' + $('#idOfGame').val(),
        success: function (data2) {
            $.each(data2, function (index, element) {
                if (element.address == 'TheEnd') {
                    document.location.href = 'finish?gameId=' + $('#idOfGame').val();
                }
            });
            $('#board2').empty();
            $('#board2').append('<img class="target" src="resources/img/seaBattle/target.png" width="40" height="40"/>' +
                '<span class="digit">1</span>' +
                '<span class="digit">2</span> ' +
                '<span class="digit">3</span>' +
                '<span class="digit">4</span> ' +
                '<span class="digit">5</span> ' +
                '<span class="digit">6</span> ' +
                '<span class="digit">7</span> ' +
                '<span class="digit">8</span> ' +
                '<span class="digit">9</span> ' +
                '<span class="digit">10</span><br/>');
            var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

            $.each(data2, function (index, element) {
                if (index % 10 == 0) {
                    var l = letters[index / 10];
                    $('#board2').append('<span class="letter" style="font-size: 26px;">' + l + '</span>');
                }
                switch (element.value) {
                    case '#':
                        $('#board2').append('<img id=\"' + element.address + '\" onclick="makeTurnClick(this)" class="celly" src="resources/img/seaBattle/anchor2.png" width="40" height="40"/>');
                        break;
                    case '_':
                        $('#board2').append('<img id=\"' + element.address + '\" onclick="makeTurnClick(this)" class="celly" src="resources/img/seaBattle/anchor2.png" width="40" height="40"/>');
                        break;
                    case 'X':
                        $('#board2').append('<img class="celly" src="resources/img/seaBattle/hit4.png" width="40" height="40"/>');
                        break;
                    case '*':
                        $('#board2').append('<img class="celly" src="resources/img/seaBattle/miss.png" width="40" height="40"/>');
                        break;
                }
                for (var x = 0; x < 10; x++) {
                    if (index == (9 + 10 * x)) {
                        $('#board2').append('<br/>');
                    }
                }
            });
        }
    });
}

function makeTurnClick(cell) {
    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: $('#idOfGame').val(), turn: cell.id},
        success: function () {
            console.log("success");
            getTip();
            refreshBoard2();
        },
        error: function () {
            alert('Make Turn Error!');
        }
    });

    /*$.ajax({
     method: 'GET',
     url: 'seabattle/makeTurn?gameId=' + $('#idOfGame').val()+ '&turn=' + cell.id,
     success: function (data3) {
     $.each(data3, function (index, element){
     if(element.address == 'TheEnd'){
     document.location.href='finish?gameId='+$('#idOfGame').val();
     }
     });
     $('#board2').empty();
     $('#board2').append('<img class="target" src="resources/img/seaBattle/target.png" width="40" height="40"/>'+
     '<span class="digit">1</span>' +
     '<span class="digit">2</span> ' +
     '<span class="digit">3</span>' +
     '<span class="digit">4</span> ' +
     '<span class="digit">5</span> ' +
     '<span class="digit">6</span> ' +
     '<span class="digit">7</span> ' +
     '<span class="digit">8</span> ' +
     '<span class="digit">9</span> ' +
     '<span class="digit">10</span><br/>');
     var letters = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

     $.each(data3, function(index, element){
     if(index%10==0){
     var l = letters[index/10];
     $('#board2').append('<span class="letter" style="font-size: 26px;">'+ l +'</span>');
     }
     switch(element.value){
     case '#': $('#board2').append('<img id=\"'+element.address +'\" onclick="makeTurnClick(this)" class="celly" src="resources/img/seaBattle/anchor2.png" width="40" height="40"/>'); break;
     case '_': $('#board2').append('<img id=\"'+element.address +'\" onclick="makeTurnClick(this)" class="celly" src="resources/img/seaBattle/anchor2.png" width="40" height="40"/>'); break;
     case 'X': $('#board2').append('<img class="celly" src="resources/img/seaBattle/hit4.png" width="40" height="40"/>'); break;
     case '*': $('#board2').append('<img class="celly" src="resources/img/seaBattle/miss.png" width="40" height="40"/>'); break;
     }
     for(var x=0; x<10; x++) {
     if (index == (9 + 10 * x)) {
     $('#board2').append('<br/>');
     }
     }
     });
     getTip();
     },
     error: function () {
     alert('Error while making makeTurn!');
     }
     });*/

}
function getTip() {
    $.ajax({
        method: 'GET',
        //url: 'seabattle/tips?id=' + $('#idOfGame').val(),
        url: 'ajax/tips?gameId=' + $('#idOfGame').val(),
        success: function (data4) {
            if (data4.message != 'GAME OVER') {
                $('#botBar').html('<div class="alert alert-dismissible alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>' + data4.message + '</div>');
            }
            else {
                window.location.replace('finish?gameId=' + $('#idOfGame').val());
            }
        },
        error: function () {
            alert('Error while printing TIP!');
        }
    });
}