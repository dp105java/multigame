<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<spring:message code="label.commonPage.versus" var="labelCommonPageVersus"/>
<spring:message code="label.commonPage.goodLuck" var="labelCommonGoodLuck"/>

<!DOCTYPE html>
<html>
<head>
    <title>Game Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/renju/renju.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/renju/refreshBoard.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/renju/doubleClickOnBoard.js"></script>
</head>
<body>

<%@include file="/WEB-INF/views/header.jsp" %>

<div class="panel-body">
    <div class="col-sm-6">

        <div class="w3-container w3-left">
            <img width=150 alt="renju.png"
                 src="${pageContext.request.contextPath}/resources/img/logo/main/renju.png">
        </div>

        <h2 style="text-align:center" class="w3-center w3-animate-top">${currentPlayer.login} ${labelCommonPageVersus}
            <a
                    href="profile?id=${opponent.id}">${opponent.login}</a>.</h2>
        <h2 style="text-align:center" class="w3-center w3-animate-top"> ${labelCommonGoodLuck}</h2>

        <br>

        <div id="tips" class="w3-left"></div>

        <input id="gameId" type="hidden" name="gameId" value="${gameId}"/>

    </div>

    <div class="col-sm-6" id="board"></div>

</div>
</body>
</html>
