<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:message code="header.rules.reversi1" var="headerRulesReversi1"/>
<spring:message code="header.rules.reversi2" var="headerRulesReversi2"/>
<spring:message code="header.rules.seaBattle1" var="headerRulesSeaBattle1"/>
<spring:message code="header.rules.seaBattle2" var="headerRulesSeaBattle2"/>
<spring:message code="header.rules.renju1" var="headerRulesRenju1"/>
<spring:message code="header.rules.renju2" var="headerRulesRenju2"/>
<spring:message code="header.rules.mill1" var="headerRulesMill1"/>
<spring:message code="header.rules.mill2" var="headerRulesMill2"/>
<spring:message code="header.rules.halma1" var="headerRulesHalma1"/>
<spring:message code="header.rules.halma2" var="headerRulesHalma2"/>
<spring:message code="header.rules.rsp1" var="headerRulesRsp1"/>
<spring:message code="header.rules.rsp2" var="headerRulesRsp2"/>
<spring:message code="header.about.reversi" var="headerAboutReversi"/>
<spring:message code="header.about.seaBattle" var="headerAboutSeaBattle"/>
<spring:message code="header.about.renju" var="headerAboutRenju"/>
<spring:message code="header.about.mill" var="headerAboutMill"/>
<spring:message code="header.about.halma" var="headerAboutHalma"/>
<spring:message code="header.about.rsp" var="headerAboutRsp"/>

<spring:message code="label.index.welcome.message" var="welcomeMessage"/>
<spring:message code="label.index.welcome.message2" var="welcomeMessage2"/>
<spring:message code="label.index.enter.login" var="enterLogin"/>
<spring:message code="label.index.enter.password" var="enterPassword"/>
<spring:message code="label.index.submit" var="submit"/>
<spring:message code="label.index.register" var="register"/>
<spring:message code="label.index.join" var="join"/>
<spring:message code="label.index.login" var="login"/>
<spring:message code="label.index.wrong.credentials" var="wrongCredentials"/>

<html>
<head>
    <title>Welcome page</title>
    <%--<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>--%>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<span id="lang-switch"><a href="?lang=en">en</a>|<a href="?lang=ru">ru</a></span>

<div id="welcome-container">
    <h2>${welcomeMessage}</h2>
    <br><br>

    <div id="welcome-text">
        ${welcomeMessage2}
    </div>
    <br>
    <div id="welcome-btn">
        <a class="btn btn-primary btn-lg btn-join" href="/multigame/list">${join}</a>
        <button type="button" class="btn btn-primary btn-lg btn-join" data-toggle="modal"
                data-target="#loginModal">${login}</button>
    </div>
    <div id="error-text">
        <c:if test="${param.error==true}">
            ${wrongCredentials}
        </c:if>
    </div>
</div>

<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal" name="f" method="post" action="/multigame/j_spring_security_check">

                    <input class="form-control" type="text" name="player_name"
                           placeholder=${enterLogin}>

                    <input class="form-control" type="password" name="player_pass"
                           placeholder=${enterPassword}>

                    <input class="btn btn-primary" type="submit" value=${submit}>

                    <a class="btn btn-success" href="registration">${register}</a>


                </form>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingReversi">
                <div class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseReversi"
                       aria-expanded="false" aria-controls="collapseReversi">
                        <img class="avatar-big"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/reversi.png"
                             alt="reversi">
                    </a>
                    <br>
                    <h4>Reversi</h4>
                </div>
            </div>
            <div id="collapseReversi" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingReversi">
                <div class="panel-body">
                    <p>${headerRulesReversi1}</p>
                    <%--<p>${headerRulesReversi2}</p>--%>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingMill">
                <div class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMill"
                       aria-expanded="false" aria-controls="collapseMill">
                        <img class="avatar-big"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/mill.png"
                             alt="mill">
                    </a><br>
                    <h4>Mill</h4>
                </div>
            </div>
            <div id="collapseMill" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingMill">
                <div class="panel-body">
                    <p>${headerRulesMill1}</p>
                    <%--<p>${headerRulesMill2}</p>--%>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingRenju">
                <div class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseRenju"
                       aria-expanded="false" aria-controls="collapseRenju">
                        <img class="avatar-big"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/renju.png"
                             alt="renju">
                    </a><br>
                    <h4>Renju</h4>
                </div>
            </div>
            <div id="collapseRenju" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingRenju">
                <div class="panel-body">
                    <p>${headerRulesRenju1}</p>
                    <%--<p>${headerRulesRenju2}</p>--%>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingHalma">
                <div class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseHalma"
                       aria-expanded="false" aria-controls="collapseHalma">
                        <img class="avatar-big"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/halma.png"
                             alt="halma">
                    </a><br>
                    <h4>Halma</h4>
                </div>
            </div>
            <div id="collapseHalma" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingHalma">
                <div class="panel-body">
                    <p>${headerRulesHalma1}</p>
                    <%--<p>${headerRulesHalma2}</p>--%>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingRSP">
                <div class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseRSP"
                       aria-expanded="false" aria-controls="collapseRSP">
                        <img class="avatar-big"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/rsp.png"
                             alt="rsp">
                    </a><br>
                    <h4>Rock Scissors Paper</h4>
                </div>
            </div>
            <div id="collapseRSP" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingRSP">
                <div class="panel-body">
                    <p>${headerRulesRsp1}</p>
                    <%--<p>${headerRulesRsp2}</p>--%>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSeaBattle">
                <div class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseBattle"
                       aria-expanded="false" aria-controls="collapseBattle">
                        <img class="avatar-big"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/seabattle.png"
                             alt="seabattle">
                    </a><br>
                    <h4>Sea Battle</h4>
                </div>
            </div>
            <div id="collapseBattle" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingSeaBattle">
                <div class="panel-body">
                    <p>${headerRulesSeaBattle1}</p>
                    <%--<p>${headerRulesSeaBattle2}</p>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="made-by">
    made by Java-Dp-105 | SoftServe IT Academy
</div>

</body>
</html>
