<%@ page import="com.softserveinc.ita.multigame.model.Game" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<spring:message code="label.finish.click" var="click"/>
<spring:message code="label.finish.here" var="here"/>
<spring:message code="label.finish.toPlay" var="toPlay"/>
<html>
<head>
    <title>Finish Page</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<%@include file="header.jsp" %>
<div class="container  brn-holder">
    <h2>${message}</h2>
    <h3>${click} <a href="${pageContext.request.contextPath}/list" style="font-size: inherit">${here} </a> ${toPlay}
    </h3>
    <div class="container jumbotron">
        <img src="${image}" style="width: 100%">
    </div>
</div>

</body>
</html>
