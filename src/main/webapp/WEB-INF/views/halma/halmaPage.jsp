<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<spring:message code="label.commonPage.versus" var="versus"/>
<spring:message code="label.commonPage.goodLuck" var="goodLuck"/>
<html>
<head>
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    <title>Halma</title>
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${contextPath}/resources/js/halma/gameBoard.js"></script>
</head>
<body>
<%@include file="/WEB-INF/views/header.jsp" %>
<input id="gameId" type="hidden" name="gameId" value="${gameId}">

<div class="w3-container w3-left">
    <img alt="rspls.png" src="${pageContext.request.contextPath}/resources/img/logo/logo_color_halma.png">
</div>

<h2 style="text-align:center" class="w3-center w3-animate-top"> ${currentPlayer.login} ${versus}
    <a href="profile?id=${opponent.id}">${opponent.login}.</a> ${color}</h2>
<h2 style="text-align:center" class="w3-center w3-animate-top"> ${goodLuck}</h2>
<br>
<br>
<div id="tips" class="w3-center"></div>
<br>
<br>
<div id="board" class="container .w3-display-middle" style="width: 850px"></div>
</body>
</html>
