<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="label.commonPage.versus" var="labelCommonPageVersus"/>
<spring:message code="label.commonPage.goodLuck" var="labelCommonGoodLuck"/>

<!DOCTYPE html>
<html>
<head>
    <title>Game Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>

<div class="w3-container w3-center"> <%--If you need to left logo, so your choice is w3-container w3-left--%>
    <img alt="rspls.png" src="${pageContext.request.contextPath}/resources/img/rsp/rspls.png">
</div>

<h2 style="text-align:center" class="w3-center w3-animate-top">${currentPlayer.login} ${labelCommonPageVersus} <a
        href="profile?id=${opponent.id}">${opponent.login}.</a></h2>
<h2 style="text-align:center" class="w3-center w3-animate-top"> ${labelCommonGoodLuck}</h2>

<div id="tips"></div>
<br>
<div id="board"></div>
<%--With your divs You can use any of CSS classes like class="w3-container w3-center"
                           just choose the position and paste this class in your div, for example,
            if you want board on the right, then your div is <div id="board" class="w3-container w3-right"></div> --%>
</body>
</html>
