<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<spring:message code="label.commonPage.youAre" var="labelCommonPageYouAre"/>
<spring:message code="label.commonPage.versus" var="labelCommonPageVersus"/>
<spring:message code="label.commonPage.goodLuck" var="labelCommonGoodLuck"/>

<html>
<head>
    <title>Mill Game</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mill/mill.css"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/mill/ajax.js"></script>
</head>
<body>

<%@include file="../header.jsp" %>

<%--<div class="w3-container w3-left"> &lt;%&ndash;If you need to left logo, so your choice is w3-container w3-left&ndash;%&gt;--%>
<%--<img alt="mill" src="${pageContext.request.contextPath}/resources/img/logo/main/mill.png">--%>
<%--</div>--%>

<h2 style="text-align:center"
    class="w3-center w3-animate-top">${labelCommonPageYouAre} ${player.login} ${labelCommonPageVersus} <a
        href="profile?id=${opponent.id}">${opponent.login}.</a></h2>
<h2 style="text-align:center" class="w3-center w3-animate-top"> ${labelCommonGoodLuck}</h2>

<hr>

<input id="gameId" type="hidden" name="gameId" value="${gameId}">
<div id="tips"></div>

<div id="field">

    <div id="white"></div>
    <div id="board">
        <div class="position" id="0"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="1"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="2"></div>
        <div class="position"></div>
        <div class="position" id="8"></div>
        <div class="position"></div>
        <div class="position" id="9"></div>
        <div class="position"></div>
        <div class="position" id="10"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="16"></div>
        <div class="position" id="17"></div>
        <div class="position" id="18"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="7"></div>
        <div class="position" id="15"></div>
        <div class="position" id="23"></div>
        <div class="position"></div>
        <div class="position" id="19"></div>
        <div class="position" id="11"></div>
        <div class="position" id="3"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="22"></div>
        <div class="position" id="21"></div>
        <div class="position" id="20"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="14"></div>
        <div class="position"></div>
        <div class="position" id="13"></div>
        <div class="position"></div>
        <div class="position" id="12"></div>
        <div class="position"></div>
        <div class="position" id="6"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="5"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="4"></div>
    </div>
    <div id="black"></div>
</div>
</body>
</html>
