<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="label.commonPage.versus" var="labelCommonPageVersus"/>
<spring:message code="label.commonPage.goodLuck" var="labelCommonGoodLuck"/>
<spring:message code="label.reversi.message" var="playingColor"/>
<spring:message code="label.reversi.result.code" var="resultCode"/>


<html>
<head>
    <title>Current Game</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reversi/reversiFieldStyle.css"/>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico"
          width="32" height="32" type="image/x-icon">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/reversi/field.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/reversi/currentpageonload.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/reversi/reversiOnClickMakeTurn.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/reversi/displayTips.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/reversi/refreshField.js"></script>
</head>
<body>

<%@include file="../header.jsp" %>

<h3 style="text-align:center" class="w3-center w3-animate-top">${player.login} ${labelCommonPageVersus}
    <a href="profile?id=${opponent.id}">${opponent.login}.</a></h3>

<div id="color" class="reversi-message">
    <p>

    <div><span>${player.login}, ${playingColor}</span></div>
    <div>
        <c:choose>
            <c:when test='${pawnColor eq "black"}'>
                <div class="black-pawn-show"></div>
            </c:when>
            <c:when test='${pawnColor eq "white"}'>
                <div class="white-pawn-show"></div>
            </c:when>
        </c:choose>
    </div>
    </p>
    <p>
    <div style="clear: left; clear: right"><span>${resultCode}: <span id="resultCode"></span></span></div>
    </p>
</div>
<div id="tips" style="text-align:center" class="well well-sm"></div>
<br>

<div id="turnField"><%--Turn--%>
    <form id="turnForm" action="maketurn" method="post">
        <input id="turn" type="hidden" name="turn"/>
    </form>
</div>


<div id="board">
    <div id="field"> <%--Field--%>
    </div>
</div>

<input type="hidden" id="idValue" value='${gameId}'/>
<input id="gameId" type="hidden" name="gameId" value="${gameId}">
<%--<input type="hidden" id="matrix" value='"${matrix}"'/>--%>


<div id="fieldBackground">
    <img src="${pageContext.request.contextPath}/resources/img/reversi/chessboard_background.jpg"
         alt="board background"/>
</div>

</body>
</html>
