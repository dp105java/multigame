<%@ page import="com.softserveinc.ita.multigame.model.Game" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    <title>Players</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</head>
<body>
<%@include file="header.jsp" %>

<div class="container jumbotron">
    <ul class="list-group">
        <c:forEach items="${players}" var="player">
            <a href="profile?id=${player.id}" style="font-size: 30px" class="list-group-item">
                <img class="avatar-small" src="${player.avatar}" alt="avatar">
                <c:choose>
                    <c:when test="${not empty player.fullName}">
                        ${player.fullName}
                    </c:when>
                    <c:otherwise>
                        ${player.login}
                    </c:otherwise>
                </c:choose>
            </a>

        </c:forEach>
    </ul>
</div>

</body>
</html>
