<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>

    <meta charset="utf-8">
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/playerProfile.js"></script>

    <title>${displayedPlayer.login}</title>
</head>
<body>

<%@include file="header.jsp" %>

<div class="container">

    <div style="float: left" class="avatar-big-positioning">
        <img src="${displayedPlayer.avatar}" class="avatar-big" alt="profile pic"/>
    </div>

    <div id="info" style="float: left" class="profile-bar-positioning">
        <h2><spring:message code="label.profile.message"/></h2>

        <div class="header"><b><spring:message code="label.profile.login"/>: </b><span
                id="login">${displayedPlayer.login}</span></div>
        <div class="header"><b><spring:message code="label.profile.fullName"/>: </b><span
                id="fullName">${displayedPlayer.fullName}</span></div>
        <div class="header"><b><spring:message code="label.profile.email"/>: </b><span
                id="email">${displayedPlayer.email}</span></div>
        <div class="header"><b><spring:message code="label.profile.gender"/>: </b><span
                id="gender">${displayedPlayer.gender}</span></div>
        <div class="header"><b><spring:message code="label.profile.birthday"/>: </b><span
                id="birthday">${displayedPlayer.birthdayDate}</span>
        </div>
        <div class="header"><b><spring:message code="label.profile.registration.time"/>: </b><span
                id="registration">${displayedPlayer.registrationTime}</span></div>

        <div>
            <c:if test='${player.login eq displayedPlayer.login}'>
                <button data-target="#updateProfile" role="button" class="btn btn-success" data-toggle="modal">
                    <spring:message code="label.profile.update"/>
                </button>
            </c:if>
        </div>
    </div>


    <c:if test='${player.login eq displayedPlayer.login}'>
        <div style="clear: left" class="container jumbotron game-list-positioning">
            <span id="history"><h2><spring:message code="label.profile.games.history"/></h2></span>
            <ul class="list-group">
                <c:forEach items="${gameHistoryList}" var="game">
                    <a href="gameHistory?gameId=${game.id}" class="list-group-item">
                        <c:out value="Game #${game.id}"/>
                    </a>
                </c:forEach>
            </ul>
        </div>
    </c:if>


</div>


<%--Update modal window--%>
<div class="modal fade" id="updateProfile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">${player.login}, <spring:message code="label.profile.modal.message"/></h4>
            </div>
            <div class="modal-body">

                <form id="profileInfoForm" class="form-inline" action="updateProfile" method="post">
                    <input type="hidden" name="login" value="${player.login}"/>

                    <div class="placeholder"><spring:message code="label.profile.modal.fullName"/>: <input
                            name="fullName" class="form-control"
                            placeholder=<spring:message code="label.profile.modal.fullName.placeholder"/>>
                    </div>

                    <div class="placeholder"><spring:message code="label.profile.modal.email"/>: <input type="email"
                                                                                                        name="email"
                                                                                                        class="form-control"
                                                                                                        placeholder=
                    <spring:message code="label.profile.modal.email.placeholder"/>>
                    </div>

                    <div class="placeholder"><spring:message code="label.profile.modal.gender"/>:
                        <select name="gender">
                            <option value="null"><spring:message code="label.profile.modal.gender.message"/></option>
                            <option value="MALE"><spring:message code="label.profile.modal.gender.male"/></option>
                            <option value="FEMALE"><spring:message code="label.profile.modal.gender.female"/></option>
                        </select>
                    </div>

                    <div class="placeholder"><spring:message code="label.profile.modal.birthday"/>:<input type="date"
                                                                                                          name="birthday"
                                                                                                          class="form-control"/>
                    </div>

                    <div style="clear: left"><input class="btn btn-success" type="submit" value=<spring:message
                            code="label.profile.modal.submit"/>></div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message
                        code="label.profile.modal.backToProfile"/></button>
            </div>
        </div>
    </div>
</div>

</body>
</html>