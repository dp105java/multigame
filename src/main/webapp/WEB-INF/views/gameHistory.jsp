<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>

    <meta charset="utf-8">
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico"
          width="32" height="32" type="image/x-icon">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

    <title>Game history #${gameId}</title>
</head>
<body>

<%@include file="header.jsp" %>

<div class="container">


    <div style="float: left" class="avatar-big-positioning">
        <c:choose>
            <c:when test='${gameType eq "REVERSI"}'>
                <img src="${pageContext.request.contextPath}/resources/img/logo/main/reversi.png"
                     class="avatar-big"
                     alt="game type pic"/>
            </c:when>
            <c:when test='${gameType eq "MILL"}'>
                <img src="${pageContext.request.contextPath}/resources/img/logo/main/mill.png" class="avatar-big"
                     alt="game type pic"/>
            </c:when>
            <c:when test='${gameType eq "HALMA"}'>
                <img src="${pageContext.request.contextPath}/resources/img/logo/main/halma.png" class="avatar-big"
                     alt="game type pic"/>
            </c:when>
            <c:when test='${gameType eq "RENJU"}'>
                <img src="${pageContext.request.contextPath}/resources/img/logo/main/renju.png" class="avatar-big"
                     alt="game type pic"/>
            </c:when>
            <c:when test='${gameType eq "SEABATTLE"}'>
                <img src="${pageContext.request.contextPath}/resources/img/logo/main/seabattle.png"
                     class="avatar-big"
                     alt="game type pic"/>
            </c:when>
            <c:when test='${gameType eq "RSP"}'>
                <img src="${pageContext.request.contextPath}/resources/img/logo/main/rsp.png" class="avatar-big"
                     alt="game type pic"/>
            </c:when>
        </c:choose>
    </div>

    <div id="info" style="float: left" class="profile-bar-positioning">
        <h2><spring:message code="label.gameHistory.game"/> # ${gameId} </h2>

        <div class="header"><b><spring:message code="label.gameHistory.gameType"/>: </b><span
                id="gameType">${gameType}</span></div>
        <div class="header"><b><spring:message code="label.gameHistory.player"/>1: </b><span
                id="player1">${gameHistory.firstPlayer}</span></div>
        <div class="header"><b><spring:message code="label.gameHistory.player"/>2: </b><span
                id="player2">${gameHistory.secondPlayer}</span></div>
        <div class="header"><b><spring:message code="label.gameHistory.startTime"/>: </b><span
                id="startTime">${gameHistory.startTime}</span></div>
        <div class="header"><b><spring:message code="label.gameHistory.endTime"/>: </b><span
                id="endTime">${gameHistory.endTime}</span></div>
        <div class="header"><b><spring:message code="label.gameHistory.gameState"/>: </b><span
                id="gameState">${gameHistory.gameState}</span></div>

        <div>
            <button data-target="#getTurns" role="button" class="btn btn-success" data-toggle="modal">
                <spring:message code="label.gameHistory.getTurns"/>
            </button>
        </div>
    </div>

</div>


<%--Turns modal window--%>
<div class="modal fade" id="getTurns">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><spring:message code="label.gameHistory.turns.story"/></h4>
            </div>
            <div class="modal-body">

                <table class="table">
                    <thead>
                    <tr>
                        <th>Player</th>
                        <th>Turn</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${gameTurns}" var="turn">
                        <tr>
                            <td>${turn.player}</td>
                            <td>${turn.turn}</td>
                            <td>${turn.time}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message
                        code="label.gameHistory.back"/></button>
            </div>
        </div>
    </div>
</div>

</body>
</html>