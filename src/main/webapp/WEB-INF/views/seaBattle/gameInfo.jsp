<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<spring:message code="label.seabattle.game" var="game"/>
<spring:message code="label.seabattle.field1" var="field1"/>
<spring:message code="label.seabattle.field2" var="field2"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Game Info</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link href="${pageContext.request.contextPath}/resources/css/seaBattle/gameinfo.css" rel="stylesheet">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/seaBattle/target.ico" width="16"
          height="16" type="image/x-icon">

    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/gameInfo.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<%@include file="/WEB-INF/views/header.jsp" %>
<div class="container-fluid bs-cont">
    <!--Top Block-->
    <div class="row bs-row">
        <h3>${game} # ${param.gameId}: <em id="you">${player.login}</em> vs. <em id="him">${anotherPlayer.login}</em>
        </h3>
    </div><!--End Of Top Block-->

    <input type="hidden" id="idOfGame" value="${param.gameId}"/>

    <!--Middle Block-->
    <div class="row bs-row-main">
        <div class="col-md-6 bs-col-c">
            <h5><img src="${player.avatar}" width="65" height="60"/></h5>
            <h4>${field1}</h4>
            <!-- Field -->
            <div class="jumbotron jumbo-c" id="board1"></div><!-- End of Field -->
        </div>

        <div class="col-md-6 bs-col-c">
            <h5><img src="${anotherPlayer.avatar}" width="65" height="60"/></h5>
            <h4>${field2}</h4>
            <!-- Score Field -->
            <div class="jumbotron jumbo-c" id="board2"></div><!-- End of Score Field -->
        </div>

    </div><!--End Of Middle Block-->

    <br/><br/><br/>

    <!--Bot Block-->
    <div class="row bs-row-bot">
        <!--Refresh Button-->
        <div class="col-md-4 bs-col">
        </div><!--End of Refresh Button-->

        <!--Warnings-->
        <div class="col-md-4 bs-col" id="botBar">
        </div> <!--End of Warnings-->

        <!--Button & Modal-->
        <div class="col-md-4 bs-col">
        </div> <!--end Button & Modal-->
    </div><!--End Of Bot Block-->

</div>

</body>
</html>
