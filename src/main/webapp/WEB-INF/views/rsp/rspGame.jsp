<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<spring:message code="label.commonPage.youAre" var="labelCommonPageYouAre"/>
<spring:message code="label.commonPage.versus" var="labelCommonPageVersus"/>
<spring:message code="label.RSPGame.goodLuck" var="labelRSPGoodLuck"/>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/rsp/rsp.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/rsp/rsp.js"></script>
    <title>Playing Game RSP</title>
</head>

<body>
<%@include file="../header.jsp" %>


<div class="w3-container w3-center">
    <img alt="rspls.png" src="${pageContext.request.contextPath}/resources/img/rsp/rspls.png">
</div>

<h2 style="text-align:center"
    class="w3-center w3-animate-bottom"><%--${labelCommonPageYouAre}--%> ${currentPlayer.login} ${labelCommonPageVersus}
    <a
            href="profile?id=${secondPlayer.getId()}">${secondPlayer.getLogin()}</a></h2>
<h2 style="text-align:center" class="w3-center w3-animate-top"> ${labelRSPGoodLuck} </h2>

<div id="gameBoard" class="w3-container w3-center">

    <div style="display: inline">
        <img src="${pageContext.request.contextPath}/resources/img/rsp/rock.jpg" alt="ROCK" id="ROCK" name="ROCK"
             class="image">
    </div>
    &nbsp;&nbsp;&nbsp;
    <div style="display: inline">
        <img src="${pageContext.request.contextPath}/resources/img/rsp/paper.jpg" alt="PAPER" id="PAPER" name="PAPER"
             class="image">
    </div>
    &nbsp;&nbsp;&nbsp;
    <div style="display: inline">
        <img src="${pageContext.request.contextPath}/resources/img/rsp/scissors.jpg" alt="SCISSORS" id="SCISSORS"
             name="SCISSORS"
             class="image">
    </div>
    &nbsp;&nbsp;&nbsp;
    <div style="display: inline">
        <img src="${pageContext.request.contextPath}/resources/img/rsp/lizard.jpg" alt="LIZARD" id="LIZARD"
             name="LIZARD"
             class="image">
    </div>
    &nbsp;&nbsp;&nbsp;
    <div style="display: inline">
        <img src="${pageContext.request.contextPath}/resources/img/rsp/spock.jpg" alt="SPOCK" id="SPOCK" name="SPOCK"
             class="image">
    </div>

</div>

<br><br>
<form id="turnForm" class="form-inline" name="turnForm" method="post" action="">
    <input id="gameId" type="hidden" name="gameId" value="${gameId}">
</form>
<div id="playerTurn" style="text-align:center">
</div>
<div id="resultCode" style="float: left;">
</div>
<br><br>
</body>
</html>
