<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <style>
        .error {
            color: #ff0000;
            font-size: medium;
        }

        .placeholder {
            clear: left;
            float: left;
        }

        .message {
            float: left;
            vertical-align: text-bottom;
        }
    </style>
</head>
<body>
<spring:message code="label.registration.login.placeholder" var="loginPlaceholder"/>
<spring:message code="label.registration.password.placeholder" var="passwordPlaceholder"/>
<spring:message code="label.registration.email.placeholder" var="emailPlaceholder"/>

<div class="container">
    <springForm:form class="form-inline" commandName="newPlayer" name="register" method="post" action="register">
        <h3><spring:message code="label.registration.message"/></h3>

        <div class="placeholder"><springForm:input path="login" cssClass="form-control"
                                                   placeholder="${loginPlaceholder}"/>
        </div>
        <div class="message"><springForm:errors path="login" cssClass="error"/></div>

        <div class="placeholder"><springForm:input path="password" cssClass="form-control"
                                                   placeholder="${passwordPlaceholder}"/>
        </div>
        <div class="message"><springForm:errors path="password" cssClass="error"/></div>

        <div class="placeholder"><springForm:input path="email" cssClass="form-control"
                                                   placeholder="${emailPlaceholder}"/>
        </div>
        <div class="message"><springForm:errors path="email" cssClass="error"/></div>

        <div style="clear: left"><input class="btn btn-success" type="submit"
                                        value=<spring:message code="label.registration.register"/>></div>

    </springForm:form>

    <button id="en" type="button" class="btn btn-default btn-xs"
            onclick="changeLocale('en')">en
    </button>
    |

    <button id="ru" type="button" class="btn btn-default btn-xs"
            onclick="changeLocale('ru')">ru
    </button>
</div>

<script>
    $(document).ready(function () {
        changeButtonState();
    });

    function changeLocale(lang) {
        var url, currentUrl = window.location.href;
        if (window.location.search == '' || window.location.search == null) {
            url = currentUrl + '?lang=' + lang;
        } else {
            url = getUrlWithoutLocale() + 'lang=' + lang;
        }
        location.href = url;
    }

    function getUrlWithoutLocale() {
        var finalUrl, data, paramsArray, params = window.location.search.substr(1);
        data = '?';
        paramsArray = params.split('&');
        $.each(paramsArray, function (index, param) {
            if (param.indexOf('lang') == -1) {
                data += param + '&';
            }
        });
        finalUrl = window.location.href.substr(0, window.location.href.indexOf('?')) + data;
        return finalUrl;
    }

    function changeButtonState() {
        var locale = getLocaleFromCookie();
        if (locale === 'en') {
            $("#en").addClass('active');
            $("#ru").removeClass('active');
        } else if (locale === 'ru') {
            $("#ru").addClass('active');
            $("#en").removeClass('active');
        }
    }

    function getLocaleFromCookie() {
        var locale = 'en', cookie = document.cookie;

        cookie = cookie.split('; ');
        $.each(cookie, function (index, elem) {
            var pair = elem.split('=');
            if (pair[0] == 'org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE') {
                locale = pair[1].toString();
            }
        });
        return locale;
    }
</script>
</body>
</html>
