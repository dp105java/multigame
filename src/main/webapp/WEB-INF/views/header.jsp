<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<spring:message code="header.information" var="headerInformation"/>
<spring:message code="header.rules" var="headerRules"/>
<spring:message code="header.rules.reversi1" var="headerRulesReversi1"/>
<spring:message code="header.rules.reversi2" var="headerRulesReversi2"/>
<spring:message code="header.rules.seaBattle1" var="headerRulesSeaBattle1"/>
<spring:message code="header.rules.seaBattle2" var="headerRulesSeaBattle2"/>
<spring:message code="header.rules.renju1" var="headerRulesRenju1"/>
<spring:message code="header.rules.renju2" var="headerRulesRenju2"/>
<spring:message code="header.rules.mill1" var="headerRulesMill1"/>
<spring:message code="header.rules.mill2" var="headerRulesMill2"/>
<spring:message code="header.rules.halma1" var="headerRulesHalma1"/>
<spring:message code="header.rules.halma2" var="headerRulesHalma2"/>
<spring:message code="header.rules.rsp1" var="headerRulesRsp1"/>
<spring:message code="header.rules.rsp2" var="headerRulesRsp2"/>
<spring:message code="header.players" var="headerPlayers"/>
<spring:message code="header.about" var="headerAbout"/>
<spring:message code="header.about.title" var="headerAboutTitle"/>
<spring:message code="header.about.reversi" var="headerAboutReversi"/>
<spring:message code="header.about.seaBattle" var="headerAboutSeaBattle"/>
<spring:message code="header.about.renju" var="headerAboutRenju"/>
<spring:message code="header.about.mill" var="headerAboutMill"/>
<spring:message code="header.about.halma" var="headerAboutHalma"/>
<spring:message code="header.about.rsp" var="headerAboutRsp"/>
<spring:message code="header.logout" var="headerLogout"/>
<spring:message code="header.gameList" var="headerGameList"/>
<spring:message code="header.profile" var="headerProfile"/>
<spring:message code="label.gameList.hello" var="labelGameListHello"/>

<nav class="navbar navbar-default" id="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="list">
                <img alt="multigame" src="${pageContext.request.contextPath}/resources/img/logo/multigame.png"
                     height="25px">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li id="gameList" class=""><a href="list"><span
                        class="glyphicon glyphicon-list"></span> ${headerGameList}</a>
                </li>
                <li id="profile" class=""><a href="profile?id=${player.id}"><span
                        class="glyphicon glyphicon-user"></span> ${headerProfile}</a></li>
                <li id="info" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">${headerInformation} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a data-toggle="modal" data-target="#rules">${headerRules}</a></li>
                        <li><a href="players">${headerPlayers}</a></li>
                        <li class="divider"></li>
                        <li><a data-toggle="modal" data-target="#about">${headerAbout}</a></li>
                    </ul>
                </li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div id="hello-player">
                        ${player.login} <img class="img-circle" src="${player.avatar}">
                    </div>
                </li>
                <li>
                    <div class="btn-locale">
                        <button id="en" type="button" class="btn btn-default btn-xs btn-block"
                                onclick="changeLocale('en')">en
                        </button>

                        <button id="ru" type="button" class="btn btn-default btn-xs btn-block"
                                onclick="changeLocale('ru')">ru
                        </button>
                    </div>
                </li>
                <%--<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> ${headerLogout}</a></li>--%>
                <li><a href="/multigame/j_spring_security_logout"><span
                        class="glyphicon glyphicon-log-out"></span> ${headerLogout}</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- About - modal window -->
<div class="modal fade" id="about">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">${headerAbout}</h4>
            </div>
            <div class="modal-body">
                <h5>${headerAboutTitle}:</h5>

                <p>${headerAboutReversi}</p>

                <p>${headerAboutSeaBattle}</p>

                <p>${headerAboutRenju}</p>

                <p>${headerAboutMill}</p>

                <p>${headerAboutHalma}</p>

                <p>${headerAboutRsp}</p>
            </div>
        </div>
    </div>
</div>

<!-- Rules - modal window -->
<div class="modal fade" id="rules">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">${headerRules}</h4>
            </div>
            <div class="modal-body">

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/reversi.png"
                             alt="reversi">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Reversi</h4>

                        <p>
                            ${headerRulesReversi1}
                        </p>

                        <p>
                            ${headerRulesReversi2}
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/mill.png"
                             alt="mill">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Mill</h4>

                        <p>
                            ${headerRulesMill1}
                        </p>

                        <p>
                            ${headerRulesMill2}
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/renju.png"
                             alt="renju">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Renju</h4>

                        <p>
                            ${headerRulesRenju1}
                        </p>

                        <p>
                            ${headerRulesRenju2}
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/halma.png"
                             alt="halma">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Halma</h4>

                        <p>
                            ${headerRulesHalma1}
                        </p>

                        <p>
                            ${headerRulesHalma2}
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/rsp.png"
                             alt="rsp">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Rock Scissors Paper</h4>
                        <p>
                            ${headerRulesRsp1}
                        </p>

                        <p>
                            ${headerRulesRsp2}
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object  rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/seabattle.png"
                             alt="seabattle">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Sea Battle</h4>

                        <p>
                            ${headerRulesSeaBattle1}
                        </p>

                        <p>
                            ${headerRulesSeaBattle2}
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        changeButtonState();
    });

    function changeLocale(lang) {
        var url, currentUrl = window.location.href;
        if (window.location.search == '' || window.location.search == null) {
            url = currentUrl + '?lang=' + lang;
        } else {
            url = getUrlWithoutLocale() + 'lang=' + lang;
        }
        location.href = url;
    }

    function getUrlWithoutLocale() {
        var finalUrl, data, paramsArray, params = window.location.search.substr(1);
        data = '?';
        paramsArray = params.split('&');
        $.each(paramsArray, function (index, param) {
            if (param.indexOf('lang') == -1) {
                data += param + '&';
            }
        });
        finalUrl = window.location.href.substr(0, window.location.href.indexOf('?')) + data;
        return finalUrl;
    }

    function changeButtonState() {
        var locale = getLocaleFromCookie();
        if (locale === 'en') {
            $("#en").addClass('active');
            $("#ru").removeClass('active');
        } else if (locale === 'ru') {
            $("#ru").addClass('active');
            $("#en").removeClass('active');
        }
    }

    function getLocaleFromCookie() {
        var locale = 'en', cookie = document.cookie;

        cookie = cookie.split('; ');
        $.each(cookie, function (index, elem) {
            var pair = elem.split('=');
            if (pair[0] == 'org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE') {
                locale = pair[1].toString();
            }
        });
        return locale;
    }
</script>