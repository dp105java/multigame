package com.softserveinc.ita.multigame.repositories;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GameHistoryRepository extends CrudRepository<GameHistory, Long> {
    @Query("select gh from GameHistory gh where gh.firstPlayer = :player or gh.secondPlayer = :player")
    List<GameHistory> findByPlayer(@Param("player") Player player);
}
