package com.softserveinc.ita.multigame.repositories;

import com.softserveinc.ita.multigame.model.TurnLog;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TurnLogRepository extends MongoRepository<TurnLog, String> {

    List<TurnLog> findByGameId(Long gameId);
}
