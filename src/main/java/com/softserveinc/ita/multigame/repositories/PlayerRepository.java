package com.softserveinc.ita.multigame.repositories;

import com.softserveinc.ita.multigame.model.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PlayerRepository extends CrudRepository<Player, Long> {
    @Query("select p from Player p where p.login = :login")
    Player findByLogin(@Param("login") String login);

    @Query("select p from Player p")
    List<Player> findAll();
}