package com.softserveinc.ita.multigame.model.engine;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.seabattle.Cell;
import org.apache.log4j.Logger;

import java.util.List;

public abstract class GenericGameEngine implements GameEngine {
    private final Long id;
    private Player firstPlayer;
    private Player secondPlayer;
    protected GameState gameState = GameState.WAIT_FOR_FIRST_PLAYER;
    private GameResultCode resultCode = GameResultCode.OK;
    private Player theWinner;
    private boolean isStarted = false;
    private static Long nextId = 0L;
    private Logger logger = Logger.getLogger(GenericGameEngine.class);

    public GenericGameEngine() {
        this(nextId++);
        if (nextId == Long.MAX_VALUE) {
            throw new RuntimeException(
                    String.format("%s has reached maximum ID value. " +
                                    "Restart the Application.",
                            this.getClass().getSimpleName()));
        }
    }

    protected GenericGameEngine(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    protected abstract boolean validateTurnLogic(String turn);

    protected abstract GameState changeGameState(Player player, String turn);

    private boolean validatePlayer(Player player) {
        return player != null && player.getId() != null && player.getLogin() != null;
    }

    private boolean validateTurnOrder(Player player) {
        boolean firstPlayerCorrectOrder = firstPlayer.equals(player)
                && (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_FIRST_PLAYER_DROP);

        boolean secondPlayerCorrectOrder = secondPlayer.equals(player)
                && (gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_DROP);

        return firstPlayerCorrectOrder || secondPlayerCorrectOrder;
    }

    @Override
    public boolean setFirstPlayer(Player player) {
        if (gameState != GameState.WAIT_FOR_FIRST_PLAYER) {
            logger.debug(this + ": firstPlayer was not set. GameResultCode.BAD_FIRST_PLAYER_ORDER");
            return errorWith(GameResultCode.BAD_FIRST_PLAYER_ORDER);
        }
        if (!validatePlayer(player)) {
            logger.debug(this + ": firstPlayer was not set. GameResultCode.BAD_PLAYER");
            return errorWith(GameResultCode.BAD_PLAYER);
        }
        firstPlayer = player;
        logger.debug(this + ": " + player + " set as firstPlayer");
        gameState = GameState.WAIT_FOR_SECOND_PLAYER;
        return ok();
    }

    @Override
    public Player getFirstPlayer() {
        return firstPlayer;
    }

    @Override
    public boolean setSecondPlayer(Player player) {
        if (gameState != GameState.WAIT_FOR_SECOND_PLAYER) {
            logger.debug(this + ": secondPlayer was not set. GameResultCode.BAD_SECOND_PLAYER_ORDER");
            return errorWith(GameResultCode.BAD_SECOND_PLAYER_ORDER);
        }
        if (!validatePlayer(player)) {
            logger.debug(this + ": secondPlayer was not set. GameResultCode.BAD_PLAYER");
            return errorWith(GameResultCode.BAD_PLAYER);
        }
        if (player.equals(firstPlayer)) {
            logger.debug(this + ": secondPlayer was not set. GameResultCode.BAD_FIRST_PLAYER_ORDER");
            return errorWith(GameResultCode.BAD_FIRST_PLAYER_ORDER);
        }
        secondPlayer = player;
        logger.debug(this + ": " + player + " set as secondPlayer");
        gameState = GameState.WAIT_FOR_FIRST_PLAYER_TURN;
        isStarted = true;
        return ok();
    }

    @Override
    public Player getSecondPlayer() {
        return secondPlayer;
    }

    @Override
    public Object getBoard(Player player) {
        throw new UnsupportedOperationException(
                String.format("%s does not use any board depends on player", this.getClass().getSimpleName()));
    }

    @Override
    public Object getBoard() {
        throw new UnsupportedOperationException(
                String.format("%s does not use any board", this.getClass().getSimpleName()));
    }

    @Override
    public boolean makeTurn(Player player, String turn) {
        if (!isStarted()) {
            logger.debug(this + ": " + player + " turn:" + turn + " unsuccessful. GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME");
            return errorWith(GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME);
        }
        if (isFinished()) {
            logger.debug(this + ": " + player + " turn:" + turn + " unsuccessful. GameResultCode.BAD_TURN_FOR_FINISHED_GAME");
            return errorWith(GameResultCode.BAD_TURN_FOR_FINISHED_GAME);
        }
        if (!validateTurnOrder(player)) {
            logger.debug(this + ": " + player + " turn:" + turn + " unsuccessful. GameResultCode.BAD_TURN_ORDER");
            return errorWith(GameResultCode.BAD_TURN_ORDER);
        }
        if (!validateTurnLogic(turn)) {
            logger.debug(this + ": " + player + " turn:" + turn + " unsuccessful. GameResultCode.BAD_TURN_LOGIC");
            return errorWith(GameResultCode.BAD_TURN_LOGIC);
        }

        gameState = changeGameState(player, turn);

        if (gameState == GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER) {
            logger.debug(this + ": " + firstPlayer + " won!");
            theWinner = firstPlayer;
        }
        if (gameState == GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER) {
            logger.debug(this + ": " + secondPlayer + " won!");
            theWinner = secondPlayer;
        }
        if (gameState == GameState.FINISHED_WITH_DRAW) {
            logger.debug(this + ": DRAW!");
            theWinner = null;
        }
        logger.debug(this + ": " + player + " turn:" + turn + " successful");
        return ok();
    }

    @Override
    public GameResultCode getResultCode() {
        return resultCode;
    }

    public GameState getGameState() {
        return gameState;
    }

    @Override
    public boolean isFinished() {
        return gameState == GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER
                || gameState == GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER
                || gameState == GameState.FINISHED_WITH_DRAW;
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }

    @Override
    public Player getTheWinner() {
        return theWinner;
    }

    @Override
    public String toString() {
        return String.format("%s #%s", this.getClass().getSimpleName(), id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GenericGameEngine that = (GenericGameEngine) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    private boolean errorWith(GameResultCode code) {
        resultCode = code;
        return false;
    }

    private boolean ok() {
        resultCode = GameResultCode.OK;
        return true;
    }

    public boolean setField(Player player, List<Cell> field) {
        return true;
    }
}
