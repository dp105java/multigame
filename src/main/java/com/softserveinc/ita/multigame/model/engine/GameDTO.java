package com.softserveinc.ita.multigame.model.engine;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@EqualsAndHashCode(of = "gameId")
public class GameDTO {
    private Long gameId;
    private GameType gameType;
    private Player owner;
    private LocalDateTime time;
    private String logo;

    public GameDTO(Game game) {
        this.gameId = game.getGameEngine().getId();
        this.gameType = game.getGameType();
        this.owner = game.getGameEngine().getFirstPlayer();
        this.time = game.getCreationTime();
        logo = "resources/img/logo/logo_color_" + gameType.getGameName() + ".png";
    }

    public Long getGameId() {
        return gameId;
    }

    public GameType getGameType() {
        return gameType;
    }

    public Player getOwner() {
        return owner;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getLogo() {
        return logo;
    }
}
