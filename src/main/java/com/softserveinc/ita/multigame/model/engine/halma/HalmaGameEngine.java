package com.softserveinc.ita.multigame.model.engine.halma;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class HalmaGameEngine extends GenericGameEngine {

    List<Cell> cells = new ArrayList<>();
    Cell cellToContinueTurn;

    private Logger logger = Logger.getLogger(HalmaGameEngine.class);

    public HalmaGameEngine() {
        //Init the board (cells)
        char[] horizontal = {'A', 'B', 'C', 'D',
                'E', 'F', 'G', 'H',
                'I', 'J', 'K', 'L',
                'M', 'N', 'O', 'P'
        };
        for (int i = 1; i <= 16; i++) {
            for (char c : horizontal) {
                cells.add(new Cell("" + c + i));
            }
        }
        initCellsState();
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        String from = turn.split(",")[0].toUpperCase();
        String to = turn.split(",")[1].toUpperCase();
        Cell fromCell = getCellByAddress(from);
        Cell toCell = getCellByAddress(to);

        char fromLetter = from.toUpperCase().charAt(0);
        char toLetter = to.toUpperCase().charAt(0);
        int fromNumber = Integer.parseInt(from.substring(1));
        int toNumber = Integer.parseInt(to.substring(1));

        if (toCell.state != CellState.EMPTY && toCell != fromCell) {
            return false;
        }

        //Player can move only his figures
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN) {
            if (fromCell.state != CellState.BUSY_BY_FIRST_PLAYER) {
                return false;
            }
        } else if (gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN) {
            if (fromCell.state != CellState.BUSY_BY_SECOND_PLAYER) {
                return false;
            }
        }

        //Player cannot jump over empty cell
        if (checkIfTurnIsAJump(from, to)) {
            logger.debug("turn is a jump");
            char letterOfOverjumpedCell;
            int numberOfOverjumpedCell;

            if (toLetter > fromLetter) {
                letterOfOverjumpedCell = (char) (toLetter - 1);
            } else if (toLetter < fromLetter) {
                letterOfOverjumpedCell = (char) (toLetter + 1);
            } else {
                letterOfOverjumpedCell = fromLetter;
            }
            if (toNumber > fromNumber) {
                numberOfOverjumpedCell = toNumber - 1;
            } else if (toNumber < fromNumber) {
                numberOfOverjumpedCell = toNumber + 1;
            } else {
                numberOfOverjumpedCell = fromNumber;
            }

            Cell overjumpedCell = new Cell("" + letterOfOverjumpedCell + numberOfOverjumpedCell);

            if (cells.get(cells.indexOf(overjumpedCell)).state == CellState.EMPTY) {
                return false;
            }
        }

        return true;
    }


    @Override
    public List<Cell> getBoard() {
        return cells;
    }

    @Override
    protected GameState changeGameState(Player player, String turn) {

        String from = turn.split(",")[0].toUpperCase();
        String to = turn.split(",")[1].toUpperCase();
        Cell fromCell = getCellByAddress(from);
        Cell toCell = getCellByAddress(to);

        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN) {
            return makeTurnDependingOnPlayer(CellState.BUSY_BY_FIRST_PLAYER, fromCell, toCell,
                    GameState.WAIT_FOR_FIRST_PLAYER_TURN, GameState.WAIT_FOR_SECOND_PLAYER_TURN);
        } else {
            return makeTurnDependingOnPlayer(CellState.BUSY_BY_SECOND_PLAYER, fromCell, toCell,
                    GameState.WAIT_FOR_SECOND_PLAYER_TURN, GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        }
    }


    private GameState makeTurnDependingOnPlayer(CellState currentPlayerCellState, Cell fromCell, Cell toCell,
                                                GameState gameStateIfContinue, GameState gameStateIfSwitch) {

        if (cellToContinueTurn == null) { // If it's new turn
            if (fromCell.equals(toCell)) {
                return gameStateIfContinue;
            }
            return makeTypicalValidTurn(fromCell, toCell, currentPlayerCellState, gameStateIfContinue, gameStateIfSwitch);
        } else {                         // If player continues turn
            //TODO maybe should be moved to validateTurnLogic()
            if (!fromCell.equals(cellToContinueTurn)) {
                return gameStateIfContinue;
            }
            if (fromCell.equals(toCell)) {
                cellToContinueTurn = null;
                return gameStateIfSwitch;
            }
            //After jump you can only jump again and can't move to nearest cell
            if (!checkIfTurnIsAJump(fromCell.address, toCell.address)) {
                return gameStateIfContinue;
            }
            return makeTypicalValidTurn(fromCell, toCell, currentPlayerCellState, gameStateIfContinue, gameStateIfSwitch);
        }
    }

    private GameState makeTypicalValidTurn(Cell fromCell, Cell toCell, CellState currentPlayerCellState,
                                           GameState gameStateIfContinue, GameState gameStateIfSwitch) {
        fromCell.setState(CellState.EMPTY);
        toCell.setState(currentPlayerCellState);

        if (checkIfGameIsFinished()) {
            switch (currentPlayerCellState) {
                case BUSY_BY_FIRST_PLAYER:
                    return GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
                case BUSY_BY_SECOND_PLAYER:
                    return GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
            }
        }

        if (checkIfTurnIsAJump(fromCell.address, toCell.address)) {
            cellToContinueTurn = toCell;
            return gameStateIfContinue;
        }
        cellToContinueTurn = null;
        return gameStateIfSwitch;
    }

    private boolean checkIfTurnIsAJump(String from, String to) {
        char fromHorizontal = from.charAt(0);
        int fromVertical = Integer.parseInt(from.substring(1));
        char toHorizontal = to.charAt(0);
        int toVertical = Integer.parseInt(to.substring(1));

        int movesVertically = Math.abs(toVertical - fromVertical);
        int movesHorizontally = Math.abs(toHorizontal - fromHorizontal);
        return movesVertically > 1 || movesHorizontally > 1;
    }

    private boolean checkIfGameIsFinished() {
        if (getCellByAddress("A1").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("A2").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("A3").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("A4").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("A5").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("B1").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("B2").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("B3").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("B4").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("B5").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("C1").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("C2").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("C3").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("C4").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("D1").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("D2").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("D3").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("E1").state == CellState.BUSY_BY_SECOND_PLAYER
                && getCellByAddress("E2").state == CellState.BUSY_BY_SECOND_PLAYER
                ) {
            return true;
        }
        if (getCellByAddress("P12").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("P13").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("P14").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("P15").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("P16").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("O12").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("O13").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("O14").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("O15").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("O16").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("N13").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("N14").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("N15").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("N16").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("M14").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("M15").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("M16").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("L15").state == CellState.BUSY_BY_FIRST_PLAYER
                && getCellByAddress("L16").state == CellState.BUSY_BY_FIRST_PLAYER
                ) {
            return true;
        }

        return false;
    }

    private void initCellsState() {
        //Init first player cells
        getCellByAddress("A1").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("A2").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("A3").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("A4").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("A5").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("B1").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("B2").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("B3").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("B4").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("B5").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("C1").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("C2").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("C3").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("C4").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("D1").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("D2").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("D3").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("E1").setState(CellState.BUSY_BY_FIRST_PLAYER);
        getCellByAddress("E2").setState(CellState.BUSY_BY_FIRST_PLAYER);

        //Init second player cells
        getCellByAddress("P12").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("P13").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("P14").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("P15").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("P16").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("O12").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("O13").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("O14").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("O15").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("O16").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("N13").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("N14").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("N15").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("N16").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("M14").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("M15").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("M16").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("L15").setState(CellState.BUSY_BY_SECOND_PLAYER);
        getCellByAddress("L16").setState(CellState.BUSY_BY_SECOND_PLAYER);
    }

    public Cell getCellByAddress(String address) {
        return cells.get(cells.indexOf(new Cell(address)));
    }
}
