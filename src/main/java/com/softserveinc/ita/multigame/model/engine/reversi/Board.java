package com.softserveinc.ita.multigame.model.engine.reversi;

import com.softserveinc.ita.multigame.model.engine.GameState;

public class Board {

    private int boardSize = 8;
    private int[][] field;
    private int whiteScore = 0;
    private int blackScore = 0;

    public Board() {
        initField();
        updateScore();
    }

    public boolean setField(int[][] field) { //should be used only for testing
        if (field.length != 8) {
            return false;
        }
        this.field = field;
        return true;
    }

    private void initField() {
        field = new int[this.boardSize][this.boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                field[i][j] = Color.EMPTY_CELL;
            }
        }
        field[3][3] = Color.BLACK; //black pawn
        field[4][4] = Color.BLACK; //black pawn
        field[4][3] = Color.WHITE; //white pawn
        field[3][4] = Color.WHITE; //white pawn
    }

    protected void updateScore() {
        int currentWhiteScore = 0;
        int currentBlackScore = 0;
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (field[i][j] == Color.WHITE) {
                    currentWhiteScore++;
                } else if (field[i][j] == Color.BLACK) {
                    currentBlackScore++;
                }
            }
        }
        this.whiteScore = currentWhiteScore;
        this.blackScore = currentBlackScore;
    }

    public int getWhiteScore() {
        return whiteScore;
    }

    public int getBlackScore() {
        return blackScore;
    }

    protected int getWinnerColor() {
        updateScore();
        return whiteScore > blackScore ? Color.WHITE : Color.BLACK;
    }

    public int[][] getField() {
        return field;
    }

    public int getBoardSize() {
        return boardSize;
    }


    /**
     * @param turn      shows user's turn in the format : letterDigit, i.e. e4; g3; A4; B2 etc.
     *                  Here could be only one letter as the first char and only one digit as the second char
     * @param gameState shows current game state (it is needed for current color selection)
     */
    public void putPawn(String turn, GameState gameState) {
        int currentColor = (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN) ? Color.WHITE : Color.BLACK;

        Turn playerTurn = new Turn(turn);
        int x = playerTurn.getKey();
        int y = playerTurn.getValue();

        field[y][x] = currentColor; //set Pawn according to the user's turn
        transformField(x, y, currentColor); //transform field (change color of pawns)
        updateScore();
    }


    //========================================================================
    //field transform part

    /**
     * @param x            shows the x coordinate of putted pawn
     * @param y            shows the y coordinate of putted pawn
     * @param currentColor shows current pawn color
     */
    protected void transformField(int x, int y, int currentColor) {

        transformHorizontals(x, y, currentColor);
        transformVerticals(x, y, currentColor);
        transformDiagonals(x, y, currentColor);

    }

    private void transformHorizontals(int x, int y, int currentColor) {
        changeRight(x, y, currentColor);
        changeLeft(x, y, currentColor);
    }

    private void transformVerticals(int x, int y, int currentColor) {
        changeUp(x, y, currentColor);
        changeDown(x, y, currentColor);
    }

    private void transformDiagonals(int x, int y, int currentColor) {
        changeUpLeft(x, y, currentColor);
        changeUpRight(x, y, currentColor);
        changeDownLeft(x, y, currentColor);
        changeDownRight(x, y, currentColor);
    }


    private void changeRight(int x, int y, int currentColor) {
        if (this.checkRight(x, y, currentColor)) {
            int max = this.getBoardSize() - 1;
            int tempValue;
            for (int i = x + 1; i <= max; i++) {
                tempValue = field[y][i];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[y][i] = currentColor;
                }

            }
        }

    }

    private void changeLeft(int x, int y, int currentColor) {
        if (this.checkLeft(x, y, currentColor)) {
            int tempValue;
            for (int i = x - 1; i >= 0; i--) {
                tempValue = field[y][i];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[y][i] = currentColor;
                }
            }
        }
    }

    private void changeUp(int x, int y, int currentColor) {
        if (this.checkUp(x, y, currentColor)) {
            int tempValue;
            for (int i = y - 1; i >= 0; i--) {
                tempValue = field[i][x];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[i][x] = currentColor;
                }
            }
        }
    }

    private void changeDown(int x, int y, int currentColor) {
        if (this.checkDown(x, y, currentColor)) {
            int max = this.getBoardSize() - 1;
            int tempValue;
            for (int i = y + 1; i <= max; i++) {
                tempValue = field[i][x];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[i][x] = currentColor;
                }
            }
        }
    }

    private void changeUpLeft(int x, int y, int currentColor) {
        if (this.checkUpLeft(x, y, currentColor)) {
            int tempValue;
            for (int i = x - 1, j = y - 1; j >= 0 && i >= 0; i--, j--) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[j][i] = currentColor;
                }
            }
        }
    }

    private void changeUpRight(int x, int y, int currentColor) {
        if (this.checkUpRight(x, y, currentColor)) {
            int max = this.getBoardSize() - 1;
            int tempValue;
            for (int i = x + 1, j = y - 1; i <= max && j >= 0; i++, j--) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[j][i] = currentColor;
                }
            }
        }
    }

    private void changeDownLeft(int x, int y, int currentColor) {
        if (this.checkDownLeft(x, y, currentColor)) {
            int max = this.getBoardSize() - 1;
            int tempValue;
            for (int i = x - 1, j = y + 1; i >= 0 && j <= max; i--, j++) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[j][i] = currentColor;
                }
            }
        }
    }

    private void changeDownRight(int x, int y, int currentColor) {
        if (this.checkDownRight(x, y, currentColor)) {
            int max = this.getBoardSize() - 1;
            int tempValue;
            for (int i = x + 1, j = y + 1; i <= max && j <= max; i++, j++) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    break;
                } else {
                    field[j][i] = currentColor;
                }
            }
        }
    }

    //============================================================
    //checking part

    private boolean checkRight(int x, int y, int currentColor) {
        int max = this.getBoardSize() - 1;
        if (x == max) {
            return false;
        }

        int opponentsColor = Color.getOpponentColor(currentColor);
        if (field[y][x + 1] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int i = x + 2; i <= max; i++) {
                tempValue = field[y][i];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }

            }
            return false;
        }
    }

    private boolean checkLeft(int x, int y, int currentColor) {
        if (x == 0) {
            return false;
        }
        int opponentsColor = Color.getOpponentColor(currentColor);

        if (field[y][x - 1] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int i = x - 2; i >= 0; i--) {
                tempValue = field[y][i];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }
            }
            return false;
        }

    }

    private boolean checkUp(int x, int y, int currentColor) {
        if (y == 0) {
            return false;
        }
        int opponentsColor = Color.getOpponentColor(currentColor);

        if (field[y - 1][x] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int j = y - 2; j >= 0; j--) {
                tempValue = field[j][x];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }
            }
            return false;
        }
    }


    private boolean checkDown(int x, int y, int currentColor) {
        int max = this.getBoardSize() - 1;
        if (y == max) {
            return false;
        }
        int opponentsColor = Color.getOpponentColor(currentColor);

        if (field[y + 1][x] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int j = y + 2; j <= max; j++) {
                tempValue = field[j][x];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }
            }
            return false;
        }
    }

    private boolean checkUpLeft(int x, int y, int currentColor) {
        if (y == 0 || x == 0) {
            return false;
        }
        int opponentsColor = Color.getOpponentColor(currentColor);

        if (field[y - 1][x - 1] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int i = x - 2, j = y - 2; i >= 0 && j >= 0; i--, j--) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }
            }
            return false;
        }
    }

    private boolean checkUpRight(int x, int y, int currentColor) {
        int max = this.getBoardSize() - 1;
        if (x == max || y == 0) {
            return false;
        }
        int opponentsColor = Color.getOpponentColor(currentColor);

        if (field[y - 1][x + 1] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int i = x + 2, j = y - 2; i <= max && j >= 0; i++, j--) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }
            }
            return false;
        }
    }

    private boolean checkDownLeft(int x, int y, int currentColor) {
        int max = this.getBoardSize() - 1;
        if (x == 0 || y == max) {
            return false;
        }
        int opponentsColor = Color.getOpponentColor(currentColor);

        if (field[y + 1][x - 1] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int i = x - 2, j = y + 2; i >= 0 && j <= max; i--, j++) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }
            }
            return false;
        }
    }

    private boolean checkDownRight(int x, int y, int currentColor) {
        int max = this.getBoardSize() - 1;
        if (x == max || y == max) {
            return false;
        }
        int opponentsColor = Color.getOpponentColor(currentColor);

        if (field[y + 1][x + 1] != opponentsColor) {
            return false;
        } else {
            int tempValue;
            for (int i = x + 2, j = y + 2; i <= max && j <= max; i++, j++) {
                tempValue = field[j][i];
                if (tempValue == currentColor) {
                    return true;
                }
                if (tempValue != opponentsColor) {
                    break;
                }
            }
            return false;
        }
    }

    //========================================================

    protected boolean hasMoreTurns() {
        int emptyCells = 0;
        boolean criteria = true;

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                if (field[i][j] == Color.EMPTY_CELL) {
                    emptyCells++;
                }
            }
        }
        if (emptyCells == 0) {
            criteria = false;
        }

        return criteria;
    }

}
