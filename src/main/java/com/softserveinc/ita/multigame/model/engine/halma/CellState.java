package com.softserveinc.ita.multigame.model.engine.halma;

public enum CellState {
    BUSY_BY_FIRST_PLAYER,
    BUSY_BY_SECOND_PLAYER,
    EMPTY
}

