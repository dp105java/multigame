package com.softserveinc.ita.multigame.model.engine.reversi;

public class Color {
    public static final int WHITE = 1;
    public static final int BLACK = 2;
    public static final int EMPTY_CELL = 0;

    public static int getOpponentColor(int currentColor) {
        return currentColor == Color.BLACK ? Color.WHITE : Color.BLACK;
    }

    public static int getColor(int colorValue) {
        if (colorValue != Color.BLACK && colorValue != Color.WHITE
                && colorValue != Color.EMPTY_CELL) {
            throw new UnsupportedOperationException();
        }
        return colorValue == Color.BLACK
                ? Color.BLACK
                : (colorValue == Color.WHITE) ? Color.WHITE : Color.EMPTY_CELL;
    }

}
