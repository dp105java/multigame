package com.softserveinc.ita.multigame.model.engine.renju;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

public class RenjuGame extends GenericGameEngine {

    private String[][] gameBoard;
    private int gameBoardSize = 15;

    public RenjuGame() {
        super();
        gameBoard = new String[gameBoardSize][gameBoardSize];
        for (int row = 0; row < gameBoard.length; row++)
            for (int col = 0; col < gameBoard[row].length; col++) {
                gameBoard[row][col] = BoardMarker.EMPTY.getBoardMarker();
            }
        gameBoard[gameBoardSize / 2][gameBoardSize / 2] = BoardMarker.BLACK.getBoardMarker();
    }

    @Override
    public String[][] getBoard() {
        return gameBoard;
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        int turnRow = getTurnRow(turn);
		if (turnRow < 0 || turnRow > 14) {
			return false;
		}
        int turnCol = getTurnCol(turn);
        if (turnCol < 0 || turnCol > 14) {
			return false;
		}
        return (gameBoard[turnRow][turnCol] == BoardMarker.EMPTY.getBoardMarker());
    }

    private int getTurnRow(String turn) {
        int separatorPosition = turn.indexOf(',');
        int turnRow = Integer.parseInt(turn.substring(0, separatorPosition));
        return turnRow;
    }

    private int getTurnCol(String turn) {
        int separatorPosition = turn.indexOf(',');
        int turnCol = Integer.parseInt(turn.substring(separatorPosition + 1, turn.length()));
        return turnCol;
    }

    @Override
    protected GameState changeGameState(Player player, String turn) {
        int turnRow = getTurnRow(turn);
        int turnCol = getTurnCol(turn);
        if (player.equals(getFirstPlayer())) {
            gameBoard[turnRow][turnCol] = BoardMarker.WHITE.getBoardMarker();
            if (gameOver(turnRow, turnCol)) {
                return GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
            } else {
                return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
            }
        } else if (player.equals(getSecondPlayer())) {
            gameBoard[turnRow][turnCol] = BoardMarker.BLACK.getBoardMarker();
            if (gameOver(turnRow, turnCol)) {
                return GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
            } else {
                return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
            }
        }
        return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
    }


    /**
     * This is called just after a piece has been played on the
     * square in the specified row and column.
     * If there are 5 squares (or more)
     * in a row in any direction, then the game is over.
     */
    private boolean gameOver(int row, int col) {

        if (count(gameBoard[row][col], row, col, 1, 0) >= 5) {
            return true;
        }
        if (count(gameBoard[row][col], row, col, 0, 1) >= 5) {
            return true;
        }
        if (count(gameBoard[row][col], row, col, 1, -1) >= 5) {
            return true;
        }
        if (count(gameBoard[row][col], row, col, 1, 1) >= 5) {
            return true;
        }

        return false;

    }

    /**
     * Counts the number of the specified player's pieces starting at
     * square (row,col) and extending along the direction specified by
     * (dirX,dirY).  It is assumed that the player has a piece at
     * (row,col).  This method looks at the squares (row + dirX, col + dirY),
     * (row + 2*dirX, col + 2*dirY), ... until it hits a square that is
     * off the board or is not occupied by one of the player's pieces.
     * It counts the squares that are occupied by the player's pieces.
     * Then, it looks in the
     * opposite direction, at squares (row - dirX, col-dirY),
     * (row - 2*dirX, col - 2*dirY), ... and does the same thing.
     * Note:  The values of dirX and dirY must be 0, 1, or -1.  At least
     * one of them must be non-zero.
     */
    private int count(String playerMarker, int row, int col, int dirX, int dirY) {

        int result = 1; // Number of pieces in a row belonging to the player.

        int rowToCheck, colToCheck;

        rowToCheck = row + dirX; // Look at square in specified direction.
        colToCheck = col + dirY;
        while (rowToCheck >= 0
                && rowToCheck < gameBoardSize
                && colToCheck >= 0
                && colToCheck < gameBoardSize
                && gameBoard[rowToCheck][colToCheck] == playerMarker) {
            // Square is on the board and contains one of the players's pieces.
            result++;
            rowToCheck += dirX; // Go on to next square in this direction.
            colToCheck += dirY;
        }

        // Look in the opposite direction.
        rowToCheck = row - dirX;
        colToCheck = col - dirY;
        while (rowToCheck >= 0
                && rowToCheck < gameBoardSize
                && colToCheck >= 0
                && colToCheck < gameBoardSize
                && gameBoard[rowToCheck][colToCheck] == playerMarker) {
            result++;
            rowToCheck -= dirX;
            colToCheck -= dirY;
        }

        return result;

    }

}
