package com.softserveinc.ita.multigame.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum GameType {
    REVERSI("reversi"),
    MILL("mill"),
    RENJU("renju"),
    HALMA("halma"),
    RSP("rsp"),
    SEABATTLE("seabattle");

    private String gameName;

    GameType(String gameName) {
        this.gameName = gameName;
    }

    @JsonValue
    public String getGameName() {
        return gameName;
    }
}
