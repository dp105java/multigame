package com.softserveinc.ita.multigame.model.engine.seabattle;

public class Cell {
    private String value;
    private String address;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
