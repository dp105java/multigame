package com.softserveinc.ita.multigame.model;

import com.softserveinc.ita.multigame.model.engine.GameState;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "games_history")
public class GameHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long gameId;
    private GameType gameType;
    private Player firstPlayer;
    private Player secondPlayer;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private GameState gameState;

    public GameHistory() {
    }

    public GameHistory(Game game) {
        this.gameId = game.getGameEngine().getId();
        this.gameType = game.getGameType();
        this.firstPlayer = game.getGameEngine().getFirstPlayer();
        this.secondPlayer = game.getGameEngine().getSecondPlayer();
        this.startTime = game.getStartTime();
        this.endTime = LocalDateTime.now();
        this.gameState = game.getGameEngine().getGameState();
    }

    @Id
    public Long getId() {
        return gameId;
    }

    public void setId(Long gameId) {
        this.gameId = gameId;
    }

    @Enumerated(EnumType.STRING)
    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    @ManyToOne
    @JoinColumn(name = "first_player_id")
    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    @ManyToOne
    @JoinColumn(name = "second_player_id")
    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @Enumerated(EnumType.STRING)
    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public void setResultCode(GameState gameState) {
        this.gameState = gameState;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GameHistory other = (GameHistory) obj;
        if (gameId == null) {
            if (other.gameId != null)
                return false;
        } else if (!gameId.equals(other.gameId))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gameId == null) ? 0 : gameId.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "GameHistory{" +
                "id=" + gameId +
                ", gameType=" + gameType.getGameName() +
                ", firstPlayer=" + firstPlayer +
                ", secondPlayer=" + secondPlayer +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", gameState=" + gameState +
                '}';
    }
}
