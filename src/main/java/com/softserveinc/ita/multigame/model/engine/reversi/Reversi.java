package com.softserveinc.ita.multigame.model.engine.reversi;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

public class Reversi extends GenericGameEngine {

    private Board board = new Board();

    public Reversi() {
        super();
    }

    public Reversi(Long id) {
        super(id);
    }

    public Board getGameBoard() {
        return this.board;
    }

    @Override
    public Object getBoard() {
        return ReversiUtil.arrayToCellsListParser(board.getField());
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        Turn currentTurn = new Turn(turn);
        int currentCellX = currentTurn.getKey();
        int currentCellY = currentTurn.getValue();
        int[][] field = board.getField();
        //check if we turn in the empty cell which is round with empty cells
        if (!cellHasNeighbours(currentCellX, currentCellY)) {
            return false;
        }
        //check if we turn in the busy cell
        if (field[currentCellY][currentCellX] != 0) {
            return false;
        }
        return true;
    }


    @Override
    protected GameState changeGameState(Player playerName, String turn) {
        //put the pawn on the board according to the player turn
        board.putPawn(turn, gameState);

        //check if someone won
        if (!board.hasMoreTurns()) {
            if (board.getWinnerColor() == Color.WHITE) {
                return GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
            } else if (board.getWinnerColor() == Color.BLACK) {
                return GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
            }
        }

        //change state if the game isn't finished
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN) {
            return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
        } else {
            return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
        }

    }

    protected Player getCurrentPlayer() {
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN) {
            return this.getFirstPlayer();
        } else if (gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN) {
            return this.getSecondPlayer();
        } else return null;
    }

    public GameState getCurrentGameState() {
        return gameState;
    }

    private boolean cellHasNeighbours(int x, int y) {
        int[][] field = board.getField();
        return hasHorizontalNeighbour(x, y, field)
                || hasVerticalNeighbours(x, y, field)
                || hasDiagonalNeighbour(x, y, field);
    }

    private boolean hasHorizontalNeighbour(int x, int y, int[][] field) {
        int max = field.length - 1;
        int plusDeltaX = (x == max) ? 0 : 1;
        int minusDeltaX = (x == 0) ? 0 : 1;
        return (field[y][x + plusDeltaX] != Color.EMPTY_CELL)
                || (field[y][x - minusDeltaX] != Color.EMPTY_CELL);
    }

    private boolean hasVerticalNeighbours(int x, int y, int[][] field) {
        int max = field.length - 1;
        int plusDeltaY = (y == max) ? 0 : 1;
        int minusDeltaY = (y == 0) ? 0 : 1;
        return (field[y - minusDeltaY][x] != Color.EMPTY_CELL)
                || (field[y + plusDeltaY][x] != Color.EMPTY_CELL);
    }

    private boolean hasDiagonalNeighbour(int x, int y, int[][] field) {
        int max = field.length - 1;
        int plusDeltaX = (x == max) ? 0 : 1;
        int minusDeltaX = (x == 0) ? 0 : 1;
        int plusDeltaY = (y == max) ? 0 : 1;
        int minusDeltaY = (y == 0) ? 0 : 1;
        return (field[y - minusDeltaY][x + plusDeltaX] != Color.EMPTY_CELL)
                || (field[y - minusDeltaY][x - minusDeltaX] != Color.EMPTY_CELL)
                || (field[y + plusDeltaY][x + plusDeltaX] != Color.EMPTY_CELL)
                || (field[y + plusDeltaY][x - minusDeltaX] != Color.EMPTY_CELL);
    }

}
