package com.softserveinc.ita.multigame.model.engine.mill;

import java.util.List;

class BoardDTO {
    private List<String> board;
    private int countOfWhite;
    private int countOfBlack;

    BoardDTO(List<String> board, int countOfWhite, int countOfBlack) {
        this.board = board;
        this.countOfWhite = countOfWhite;
        this.countOfBlack = countOfBlack;
    }

    public List<String> getBoard() {
        return board;
    }

    public int getCountOfWhite() {
        return countOfWhite;
    }

    public int getCountOfBlack() {
        return countOfBlack;
    }
}
