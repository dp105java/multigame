package com.softserveinc.ita.multigame.model.engine;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.seabattle.Cell;

import java.util.List;

public interface GameEngine {
    Long getId();

    boolean setFirstPlayer(Player player);

    Player getFirstPlayer();

    boolean setSecondPlayer(Player player);

    Player getSecondPlayer();

    boolean makeTurn(Player player, String turn);

    GameResultCode getResultCode();

    GameState getGameState();

    boolean isFinished();

    boolean isStarted();

    Object getBoard(Player player);

    Object getBoard();

    Player getTheWinner();

    boolean setField(Player player, List<Cell> field);
}
