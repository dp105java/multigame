package com.softserveinc.ita.multigame.model;

import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@ToString(exclude = "id")
public class TurnLog {

    @Id
    private String id;
    private Long gameId;
    private String player;
    private String turn;
    private LocalDateTime time;

    public TurnLog() {
    }

    public TurnLog(Long gameId, String player, String turn) {
        this.gameId = gameId;
        this.player = player;
        this.turn = turn;
        this.time = LocalDateTime.now();
    }

    public Long getGameId() {
        return gameId;
    }

    public String getPlayer() {
        return player;
    }

    public String getTurn() {
        return turn;
    }

    public LocalDateTime getTime() {
        return time;
    }
}
