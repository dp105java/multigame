package com.softserveinc.ita.multigame.model.engine.renju;

public enum BoardMarker {
    EMPTY(""), BLACK("B"), WHITE("W");

    private String boardMarker;

    BoardMarker(String s) {
        boardMarker = s;
    }

    public String getBoardMarker() {
        return boardMarker;
    }
}
