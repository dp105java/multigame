package com.softserveinc.ita.multigame.model;

import java.util.List;

public interface GameListManager {
    Game getGame(Long id);

    boolean createGame(Player firstPlayer, GameType gameType);

    Game createAndReturnGame(Player firstPlayer, GameType gameType);

    boolean deleteGame(Long id);

    List<Long> getCreatedGamesIds(Player player);

    List<Long> getPlayingGamesIds(Player player);

    List<Long> getWaitingGamesIds(Player player);

    List<Long> getAllIds();
}
