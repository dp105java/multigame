package com.softserveinc.ita.multigame.model.engine.reversi;

public class Cell {

    private int x;
    private int y;
    private int value;

    public Cell(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "{\"x\": " + x + ", \"y\": " + y + ", \"value\": " + value + "}";
    }
}
