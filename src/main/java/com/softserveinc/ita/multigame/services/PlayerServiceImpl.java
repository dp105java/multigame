package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    GameHistoryService gameHistoryService;

    @Transactional(readOnly = true)
    @Override
    public Player get(Long id) {
        if (id == null) {
            return null;
        }
        return playerRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Player getByLogin(String login) {
        if (login == null) {
            return null;
        }
        return playerRepository.findByLogin(login);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Player> getAll() {
        return playerRepository.findAll();
    }

    @Override
    public void delete(Player player) {
        if (player == null) return;
        gameHistoryService.removeGameHistoriesForPlayer(player);
        playerRepository.delete(player);
    }

    @Override
    public void delete(Long id) {
        Player player = get(id);
        delete(player);

    }

    @Override
    public Player saveOrUpdate(Player player) {
        if (player == null) {
            return null;
        }

        if (player.getId() == null && player.getLogin() != null
                && getByLogin(player.getLogin()) != null) {
            return null;
        }

        return playerRepository.save(player);
    }
}
