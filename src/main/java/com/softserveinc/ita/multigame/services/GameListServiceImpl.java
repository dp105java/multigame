package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameDTO;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Service
public class GameListServiceImpl implements GameListService {
    @Autowired
    private GameListManager gameListManager;//= new GameListManagerImpl();
    @Autowired
    GameHistoryService gameHistoryService;
    private Game game;


    @Override
    public void createGame(Player firstPlayer, GameType gameType) {
        gameListManager.createGame(firstPlayer, gameType);
    }

    @Override
    public Game createAndReturnGame(Player firstPlayer, GameType gameType) {
        return gameListManager.createAndReturnGame(firstPlayer, gameType);
    }

    @Override
    public void deleteGame(Long gameId) {
        gameListManager.deleteGame(gameId);
    }

    @Override
    public Game getGame(Long gameId) {
        return gameListManager.getGame(gameId);
    }

    @Override
    public List<GameDTO> getPlayingGamesDTOs(Player player) {
        return gameListManager.getPlayingGamesIds(player)
                .stream()
                .map(id -> new GameDTO(gameListManager.getGame(id)))
                .collect(Collectors.toList());
    }

    @Override
    public List<GameDTO> getCreatedGamesDTOs(Player player) {
        return gameListManager.getCreatedGamesIds(player)
                .stream()
                .map(id -> new GameDTO(gameListManager.getGame(id)))
                .collect(Collectors.toList());
    }

    @Override
    public List<GameDTO> getWaitingGamesDTOs(Player player) {
        return gameListManager.getWaitingGamesIds(player)
                .stream()
                .map(id -> new GameDTO(gameListManager.getGame(id)))
                .collect(Collectors.toList());
    }

    @Override
    public boolean makeTurn(Long gameId, Player player, String turn) {
        game = gameListManager.getGame(gameId);
        boolean isTurnOk = game.makeTurn(player, turn);
        if (!isTurnOk) {
            return false;
        }

        if (game.getGameEngine().isFinished()) {
            gameHistoryService.saveGameHistoryForGame(game);
            gameListManager.deleteGame(gameId);
        }
        return true;
    }

    @Override
    public TipDTO getTips(Long gameId, Player currentPlayer) {
        Game game = gameListManager.getGame(gameId);
        if (game == null) {
            return new TipDTO(null, "GAME OVER");
        }
        GameState gameState = game.getGameEngine().getGameState();
        Player firstPlayer = game.getGameEngine().getFirstPlayer();
        Player secondPlayer = game.getGameEngine().getSecondPlayer();

        Player opponent = null;
        String rbSuffix = "";
        if (currentPlayer.equals(firstPlayer)) {
            opponent = secondPlayer;
            rbSuffix = "_FP";
        } else if (currentPlayer.equals(secondPlayer)) {
            opponent = firstPlayer;
            rbSuffix = "_SP";
        } else {
            return new TipDTO(opponent, "It's not your game!");
        }

        Locale currentLocale = LocaleContextHolder.getLocale();
        ResourceBundle resourceBundle = ResourceBundle.getBundle("tips_message", currentLocale);

        String message = "";
        String opponentLogin = ((opponent == null) ? "opponent" : opponent.getLogin());

        switch (gameState) {
            case WAIT_FOR_FIRST_PLAYER_TURN:
            case WAIT_FOR_SECOND_PLAYER_TURN:
            case WAIT_FOR_FIRST_PLAYER_DROP:
            case WAIT_FOR_SECOND_PLAYER_DROP:
            case FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER:
            case FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER:
                message = MessageFormat.format(resourceBundle.getString(gameState.name() + rbSuffix),
                            currentPlayer.getLogin(), opponentLogin);
                break;
            default:
                message = resourceBundle.getString(gameState.name());
                break;
        }

        return new TipDTO(opponent, message);
    }
}
