package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameDTO;

import java.util.List;

public interface GameListService {

    void createGame(Player firstPlayer, GameType gameType);

    Game createAndReturnGame(Player firstPlayer, GameType gameType);

    void deleteGame(Long gameId);

    Game getGame(Long gameId);

    List<GameDTO> getPlayingGamesDTOs(Player player);

    List<GameDTO> getCreatedGamesDTOs(Player player);

    List<GameDTO> getWaitingGamesDTOs(Player player);

    boolean makeTurn(Long gameId, Player player, String turn);

    TipDTO getTips(Long gameId, Player currentPlayer);
}
