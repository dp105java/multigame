package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;

public class TipDTO {
    private Player opponent;
    private String message;

    public TipDTO(Player opponent, String message) {
        this.opponent = opponent;
        this.message = message;
    }

    public Player getOpponent() {
        return opponent;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
