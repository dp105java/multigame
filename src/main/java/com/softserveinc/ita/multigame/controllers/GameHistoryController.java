package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.TurnLog;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class GameHistoryController {

    private Logger logger = Logger.getLogger(GameHistoryController.class);
    private GameHistoryService gameHistoryService;

    @Autowired
    public GameHistoryController(GameHistoryService gameHistoryService) {
        this.gameHistoryService = gameHistoryService;
    }
    @RequestMapping(value = "/gameHistory", method = RequestMethod.GET)
    public String getGameHistory(@RequestParam("gameId") Long gameId, ModelMap map) {

        GameHistory gameHistory = gameHistoryService.getGameHistoryById(gameId);

        GameType gameType = gameHistory.getGameType();
        List<TurnLog> gameTurns = gameHistoryService.getTurnsByGameId(gameId);

        map.put("gameId", gameId);
        map.put("gameType", gameType);
        map.put("gameHistory", gameHistory);
        map.put("gameTurns", gameTurns);
        logger.info("redirect to gameHistory.jsp");
        return "gameHistory";
    }

}
