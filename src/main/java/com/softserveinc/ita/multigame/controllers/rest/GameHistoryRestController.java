package com.softserveinc.ita.multigame.controllers.rest;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.TurnLog;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.PlayerService;

/**
* RestController to get information about finished games (game histories)
* @author Artem Dvornichenko artem.dvornichenko@gmail.com advorntc
*
*	GET		/api/v.0.1/gameHistories/playerId	get list of game histories for player
*	GET		/api/v.0.1/gameHistory/id			get game history by game id
*	GET		/api/v.0.1/turnLog/id				get list of turns for game
*	GET		/api/v.0.1/gameWinner/id			get game winner
*	GET		/api/v.0.1/gameLoser/id				get game loser
*
*/

@RestController
@RequestMapping("/api/v.0.1")
public class GameHistoryRestController {

	private Logger logger = Logger.getLogger(GameHistoryRestController.class);
	@Autowired
	private GameHistoryService gameHistoryService;
	@Autowired
	private PlayerService playerService;

	@GetMapping(value = "/gameHistories/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<GameHistory>> getGameHistoriesForPlayer(@PathVariable("playerId") Long playerId) {
		Player player = playerService.get(playerId);

		if (player == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		List<GameHistory> gameHistories = gameHistoryService.getGameHistoriesForPlayer(player);
		return new ResponseEntity<>(gameHistories, HttpStatus.OK);
	}

	@GetMapping(value = "/gameHistory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GameHistory> getGameHistory(@PathVariable("id") Long gameId) {
		GameHistory gameHistory = gameHistoryService.getGameHistoryById(gameId);

		if (gameHistory == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(gameHistory, HttpStatus.OK);
	}
	
	@GetMapping(value = "/turnLog/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TurnLog>> getTurnLog(@PathVariable("id") Long gameId) {
		List<TurnLog> turnLog = gameHistoryService.getTurnsByGameId(gameId);

		if (turnLog == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(turnLog, HttpStatus.OK);
	}
	
	@GetMapping(value = "/gameWinner/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Player> getGameWinner(@PathVariable("id") Long gameId) {
		Player gameWinner = gameHistoryService.getWinner(gameId);

		if (gameWinner == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(gameWinner, HttpStatus.OK);
	}
	
	@GetMapping(value = "/gameLoser/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Player> getGameLoser(@PathVariable("id") Long gameId) {
		Player gameLoser = gameHistoryService.getLoser(gameId);

		if (gameLoser == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(gameLoser, HttpStatus.OK);
	}

}
