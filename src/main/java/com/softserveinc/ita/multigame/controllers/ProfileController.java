package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

@Controller
public class ProfileController {

    private Logger logger = Logger.getLogger(ProfileController.class);


    private PlayerService playerService;
    private GameHistoryService gameHistoryService;

    public ProfileController(PlayerService playerService, GameHistoryService gameHistoryService) {
        this.gameHistoryService = gameHistoryService;
        this.playerService = playerService;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profilePage(@RequestParam("id") Long playerId, ModelMap map) {

        Player player = playerService.get(playerId);

        List<GameHistory> gameHistoryList = gameHistoryService.getGameHistoriesForPlayer(player);

        map.put("displayedPlayer", player);
        map.put("gameHistoryList", gameHistoryList);

        return "playerProfile";
    }

    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
    public String updateProfile(HttpServletRequest request, ModelMap map) {
        Player player = playerService.getByLogin(request.getParameter("login"));
        player.setFullName(request.getParameter("fullName"));
        player.setEmail(request.getParameter("email"));
        if (!request.getParameter("gender").equals("null")) {
            player.setGender(Gender.valueOf(request.getParameter("gender")));
        }
        if (!request.getParameter("birthday").equals("") || request.getParameter("birthday").equals("\'\'")) {
            player.setBirthdayDate(LocalDate.parse(request.getParameter("birthday")));
        }

        playerService.saveOrUpdate(player);
        logger.info("updated player " + player.getLogin());

        List<GameHistory> gameHistoryList = gameHistoryService.getGameHistoriesForPlayer(player);
        map.put("displayedPlayer", player);
        map.put("gameHistoryList", gameHistoryList);

        return "redirect:/profile?id=" + player.getId();
    }

    @RequestMapping("/players")
    public String getAllProfiles(ModelMap map) {
        List<Player> players = playerService.getAll();
        map.put("players", players);

        return "playersList";
    }
}
