package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.TipDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ajax")
public class GameAjaxController {
    private Logger logger = Logger.getLogger(GameAjaxController.class);


    GameListService gameListService;
    @Autowired
    public GameAjaxController(GameListService gameListService) {
        this.gameListService = gameListService;
    }

    @RequestMapping(value = "/board", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getBoard(@RequestParam("gameId") Long gameId) {
        logger.info("In getBoard gameId is: " + gameId);
        Game game = gameListService.getGame(gameId);
        if (game == null) {
            return null;
        }
        return game.getGameEngine().getBoard();
    }

    @RequestMapping(value = "/tips", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TipDTO getTips(@RequestParam("gameId") Long gameId, @SessionAttribute Player player) {
        logger.info("In getTips gameId is: " + gameId);
        return gameListService.getTips(gameId, player);
    }

    @RequestMapping(value = "/maketurn", method = RequestMethod.POST)
    public void makeTurn(@RequestParam("gameId") Long gameId,
                         @RequestParam("turn") String turn,
                         @SessionAttribute Player player) {
        logger.info("In makeTurn gameId is: " + gameId + " turn is: " + turn);
        gameListService.makeTurn(gameId, player, turn);
    }

}
