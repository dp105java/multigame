package com.softserveinc.ita.multigame.controllers.rest;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.util.UriComponentsBuilder;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameDTO;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;

/**
* RestController for CRUD methods with games
* @author Artem Dvornichenko artem.dvornichenko@gmail.com advorntc
*
*	GET		/api/v.0.1/createdGames/playerId	get list of created games for player
*	GET		/api/v.0.1/waitingGames/playerId	get list of waiting games for player
*	GET		/api/v.0.1/playingGames/playerId	get list of playing games for player
*	GET		/api/v.0.1/game/id					get game by id
*	PUT		/api/v.0.1/game						add new game
*	DELETE	/api/v.0.1/game/id					delete game by id
*	POST	/api/v.0.1/join/id					join to waiting game
*
*/

@RestController
@RequestMapping("/api/v.0.1")
public class GameRestController {

	private Logger logger = Logger.getLogger(GameRestController.class);
	@Autowired
	private GameListService gameListService;
	@Autowired
	private PlayerService playerService;

	@GetMapping(value = "/createdGames/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<GameDTO>> getCreatedGames(@PathVariable("playerId") Long playerId) {
		Player player = playerService.get(playerId);

		if (player == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		List<GameDTO> createdGamesList = gameListService.getCreatedGamesDTOs(player);
		return new ResponseEntity<>(createdGamesList, HttpStatus.OK);
	}

	@GetMapping(value = "/waitingGames/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<GameDTO>> getWaitingGames(@PathVariable("playerId") Long playerId) {
		Player player = playerService.get(playerId);

		if (player == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		List<GameDTO> waitingGamesList = gameListService.getWaitingGamesDTOs(player);
		return new ResponseEntity<>(waitingGamesList, HttpStatus.OK);
	}

	@GetMapping(value = "/playingGames/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<GameDTO>> getPlayingGames(@PathVariable("playerId") Long playerId) {
		Player player = playerService.get(playerId);

		if (player == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		List<GameDTO> playingGamesList = gameListService.getPlayingGamesDTOs(player);
		return new ResponseEntity<>(playingGamesList, HttpStatus.OK);
	}

	@GetMapping(value = "/game/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Game> getGame(@PathVariable("id") Long gameId) {
		Game game = gameListService.getGame(gameId);

		if (game == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(game, HttpStatus.OK);
	}

	@PutMapping(value = "/game", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> createGame(@RequestBody String gameType, @SessionAttribute Player player,
			BindingResult bindingResult, UriComponentsBuilder builder) {
		if (bindingResult.hasErrors()) {
			logger.error("HttpStatus: BAD_REQUEST");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		gameListService.createGame(player, GameType.valueOf(gameType));
		logger.info("HttpStatus: CREATED");

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/game/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> deleteGame(@PathVariable("id") Long gameId) {
		gameListService.deleteGame(gameId);
		logger.info("HttpStatus: OK");
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/join/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> joinGame(@PathVariable("id") Long gameId, @SessionAttribute Player player) {

		Game game = gameListService.getGame(gameId);
		if (game == null) {
			logger.error("HttpStatus: NO_CONTENT");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		if (game.getGameType() == GameType.SEABATTLE) {
			logger.error("HttpStatus: BAD_REQUEST_SEABATTLE");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (game.joinToGame(player)) {
			logger.info("HttpStatus: OK_JOINED");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		logger.info("HttpStatus: BAD_REQUEST");
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
