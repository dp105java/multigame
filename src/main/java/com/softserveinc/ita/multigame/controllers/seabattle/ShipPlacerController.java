package com.softserveinc.ita.multigame.controllers.seabattle;

import com.softserveinc.ita.multigame.controllers.GameListController;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.seabattle.Cell;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.SetFieldService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
public class ShipPlacerController {
    private Logger logger = Logger.getLogger(GameListController.class);
    @Autowired
    private GameListService gameListService;
    @Autowired
    private SetFieldService setFieldService;

    @RequestMapping(value = "/createPersonal", method = RequestMethod.GET)
    public String createSeaBattleWithPlacing(@SessionAttribute("player") Player player, ModelMap map) throws IOException {
        Game game = gameListService.createAndReturnGame(player, GameType.SEABATTLE);
        logger.info("CreateGame gameType is: " + GameType.SEABATTLE.getGameName());
        map.addAttribute("gameId", game.getGameEngine().getId());
        return "seaBattle/setShips";
    }

    @ResponseBody
    @RequestMapping(value = "/printEmpty", method = RequestMethod.GET)
    public List<Cell> printEmptyField() throws IOException {
        return setFieldService.getEmptyField();
    }

    @ResponseBody
    @RequestMapping(value = "/confirm")
    public String checkField(@SessionAttribute("player") Player player, @RequestBody FieldDTO info) throws IOException {
        Game game = gameListService.getGame(info.getId());
        List<Cell> field = info.getField();
        if (setFieldService.validateField(field)) {
            if (player.equals(game.getGameEngine().getFirstPlayer())) {
                game.getGameEngine().setField(player, field);
                return "field is ok";
            } else {
                game.joinToGame(player);
                game.getGameEngine().setField(player, field);
                return "field is ok";
            }
        } else return "field is invalid";
    }

    @RequestMapping(value = "/default", method = RequestMethod.GET)
    public String defaultShipLocation(@SessionAttribute("player") Player player, @RequestParam("gameId") Long gameId) throws IOException {
        Game game = gameListService.getGame(gameId);
        if (player.equals(game.getGameEngine().getFirstPlayer()))
            return "redirect:/list";
        else {
            game.joinToGame(player);
            return "redirect:/list";
        }
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.GET)
    public String refreshSetPage(@RequestParam("gameId") Long gameId, Model model) throws IOException {
        model.addAttribute("gameId", gameId);
        return "seaBattle/setShips";
    }
}
