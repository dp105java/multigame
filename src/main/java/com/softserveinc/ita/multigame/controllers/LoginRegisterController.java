package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.PlayerService;
import com.softserveinc.ita.multigame.services.utils.PasswordEncryptor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
@SessionAttributes("player")
public class LoginRegisterController {

    private Logger logger = Logger.getLogger(LoginRegisterController.class);

    @Autowired
    PlayerService playerService;

    @RequestMapping("/")
    public String startPage() {
        return "index";
    }

    @RequestMapping("/registration")
    public String registerPage() {
        return "registration";
    }

    @ModelAttribute("newPlayer")
    public Player createPlayerModel() {
        return new Player();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerNewPlayer(@ModelAttribute("newPlayer") @Valid Player player,
                                    BindingResult bindingResult,
                                    ModelMap map) {

        if (bindingResult.hasErrors()) {
            logger.info("Errors during field fulfilling. Returning to registration form");
            return "registration";
        }
        if (playerService.getByLogin(player.getLogin()) == null) {
            String password = player.getPassword();
            player.setPassword(PasswordEncryptor.getEncryptedPassword(password));
            player.setRegistrationTime(LocalDateTime.now());
            player.setAvatar("http://localhost:8080/multigame/resources/avatars/default.png");
            playerService.saveOrUpdate(player);
            logger.info("Success registration form fulfilling. Returning to login page");
            map.addAttribute("player", player);
        }
        return "redirect:/";

    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        return "redirect:/registration";
    }

    @RequestMapping(value = "/login")
    public String loginSuccess(ModelMap map, HttpServletRequest request) {
        String login = request.getRemoteUser();
        Player player = playerService.getByLogin(login);
        map.addAttribute("player", player);
        return "redirect:/list";
    }
}
